<?php

	/********************************************************************
 * 
 * Choosen booked customer
 * => Allow to choose which reservation to do check-in
 * 
 *********************************************************************/

define('TITLE', 'Check in');
session_start();
include '../include/manager_header.html';
include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) { 

// allow the user to choose the room
if (isset($_POST['submit'])) { 
	$_SESSION['roomType'] = array($_POST['rt1'],$_POST['rt2'],$_POST['rt3'],$_POST['rt4']);
	$_SESSION['reservation'] = $_POST['id'];
	header('Location: cin_choose_room.php');
} 

else {

	 	// choose the reservation
			$query = 'SELECT re.id, re.add_by, re.guest_id, re.rt1, re.rt2, re.rt3, re.rt4, re.in_date, re.out_date, 
			re.total, c.symbol, re.special_offer, g.id as gid, g.title, g.fname, g.lname, g.phone_no, g.passport_no 
			from reservation re JOIN guest g ON re.guest_id = g.id JOIN currency_type c ON re.currency_type_id = c.id WHERE re.active = 1 ORDER BY re.id';
			
				if($r = mysqli_query($dbc, $query)){
					$count = 0;

					print '<center><h1>Choose the reservation</h1></center>
					<div style="margin-top: 20px;">
					<table width="1000px;">
					<tr>
					<th>Number</th>
					<th>Made by</th>
					<th>Guest Name</th>
					<th>Invoice ID</th>
					<th>STANDARD SINGLE</th>
					<th>DELUXE DOUBLE</th>
					<th>SUPERIOR TWIN</th>
					<th>FAMILY SUITE</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Comment</th>
					<th>Total</th>
				</tr>';

				/************************************************
				 * 
				 * Print all the record to the screen for choosing
				 * 
				 ***********************************************/
				
				while ($row = mysqli_fetch_array($r)) {
					$count++;
					print "<form action=\"cin_booked_cus.php\" method=\"post\">
									<tr>
										<td>$count</td>
										<td>{$row['add_by']}</td>
										<td><a href=\"../guest/check_guest.php?id={$row['gid']}\">{$row['title']} {$row['fname']} {$row['lname']}</a></td>
										<td><a href=\"../invoice/reservation_info.php?id={$row['id']}\">{$row['id']}</a></td>
										<td>{$row['rt1']}</td>
										<td>{$row['rt2']}</td>
										<td>{$row['rt3']}</td>
										<td>{$row['rt4']}</td>
										<td>{$row['in_date']}</td>
										<td>{$row['out_date']}</td>
										<td>{$row['special_offer']}</td>
										<td>{$row['total']} {$row['symbol']}</td>
										<input type=\"hidden\" name=\"rt1\" value=" .$row['rt1']. ">
										<input type=\"hidden\" name=\"rt2\" value=" .$row['rt2']. ">
										<input type=\"hidden\" name=\"rt3\" value=" .$row['rt3']. ">
										<input type=\"hidden\" name=\"rt4\" value=" .$row['rt4']. ">
										<input type=\"hidden\" name=\"id\" value=" .$row['id']. ">
										<td><input type=\"submit\" name=\"submit\" value=\"Next\" /></td>
								</tr></form>";
				}
				print '</table>'; 

				if ($count == 0){
							echo '<p style="color: red;">There is no data right now!</p>';
				}
				
				print '<div style="margin-top: 100px;">';	
		} else { 
			print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
			<p>The query being run was: ' . $query . '</p>';
		}
} 
	mysqli_close($dbc);
	include '../include/footer.html';

} else {
	header('Location: ../login/manager_login.php');
}
?>