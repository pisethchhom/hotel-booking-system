<?php

	define('TITLE', 'Payment');
	session_start();
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) {
	if (isset($_SESSION['reservation'])){
		$query = "SELECT re.id, re.guest_id as gid, re.rt1, re.rt2, re.rt3, re.rt4, re.in_date, re.out_date, re.total, 
		g.title, g.fname, g.lname, g.email From reservation re JOIN guest g ON re.guest_id = g.id JOIN currency_type c ON re.currency_type_id = c.id
		WHERE ((re.id = {$_SESSION['reservation']}) AND (re.active = 1))";

		$r = mysqli_query($dbc, $query);
		$row =  mysqli_fetch_array($r);
		$_SESSION['guest'] = array($row['title'], $row['fname'], $row['lname'], $row['email']);
		$_SESSION['date'] = array($row['in_date'], $row['out_date']);
		$_SESSION['total'] = $row['total'];
	} else {
		$i = 1;
		$total = 0;
		foreach ($_SESSION['roomType'] as $room){
			if (($room > 0) && ($i < 5)){
				$room_query = "SELECT rt.name, rt.price, ct.symbol, ct.id as symbol_id FROM room_type rt 
				JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = $i AND rt.active = 1)";
				$r = mysqli_query($dbc, $room_query);
				$row = mysqli_fetch_array($r);
				$price = ($row['price'] * $room) * $_SESSION['date'][2];
				$total += $price;
			}
			++$i;
		}
		$_SESSION['total'] = $total;
	}

		if (isset($_POST['submit'])) {
				$check_in =  mysqli_real_escape_string($dbc, $_SESSION['date'][0]);
				$check_out =  mysqli_real_escape_string($dbc, $_SESSION['date'][1]);
				$discount = floatval($_POST['discount']);
				$total = floatval($_SESSION['total']);
				$cash_in = floatval($_POST['cash_in']);
				$cash_out = floatval($_POST['cash_out']);
				
				$errors = array();

				// check money
				if (($discount > 100) || ($discount < 0))	{array_push($errors, 'Discount is invalid amount!');}
		
				elseif (!empty($discount)) {
					$total = $total * (100-$discount) / 100;
				}
		
				if ($cash_in < $total) {array_push($errors, 'Cash in is less than '.$total.'$!');}

				elseif ($cash_out > ($cash_in - $total)) {array_push($errors, 'Cash out should be '.($cash_in - $total).'$!');}
				// end of checking

				if (count($errors) == 0) {
					if (isset($_SESSION['reservation'])){
						$guest_query = "UPDATE guest set stay = 1 WHERE id = {$row['gid']}";

						$reservation_update = "UPDATE reservation set active = 0 WHERE id = {$row['id']}";
						mysqli_query($dbc, $reservation_update);

						$email = mysqli_real_escape_string($dbc, $row['email']);

					} else {
						//$_SESSION['guest'] = array($title,$fname,$lname,$email,$phone_no,$pass_no,$nationality);
						$guest_query = "INSERT INTO guest VALUES(NULL,NULL,'{$_SESSION['guest'][0]}','{$_SESSION['guest'][1]}','{$_SESSION['guest'][2]}'
						,'{$_SESSION['guest'][3]}','{$_SESSION['guest'][4]}','{$_SESSION['guest'][5]}','{$_SESSION['guest'][6]}',1,1)" ;

						$email = mysqli_real_escape_string($dbc, $_SESSION['guest'][3]);

						$start_query = 'SELECT id FROM timetable WHERE ((month_id = '.(int)date('m').') AND (date_num = '.(int)date('d').') AND (active = 1))';
						$r_start = mysqli_query($dbc, $start_query);
						$start = mysqli_fetch_array($r_start); // start from id of start date
						$id = $start[0];

						for ($i=0; $i<$_SESSION['date'][2]; $i++){
							$query = "UPDATE timetable SET rt1 = rt1 + {$_SESSION['roomType'][0]}, rt2 = rt2 + {$_SESSION['roomType'][1]}, 
							rt3 = rt3 + {$_SESSION['roomType'][2]}, rt4 = rt4 + {$_SESSION['roomType'][3]} WHERE ((id = $id) AND (active = 1))";
							mysqli_query($dbc, $query);
							++$id;
						}
					}					
						mysqli_query($dbc, $guest_query);

						$guest = "SELECT id from guest WHERE email = '$email'";
						$r = mysqli_query($dbc, $guest);
						$row_guest = mysqli_fetch_array($r);

						$payment_query = "INSERT INTO payment VALUES(NULL,NULL,{$row_guest['id']},$discount,$total,$cash_in,$cash_out,2,1)";
						if (mysqli_query($dbc, $payment_query)){}else echo $payment_query;

						$payment = "SELECT id FROM payment WHERE ((guest_id = {$row_guest['id']}) AND (active = 1))";
						$r = mysqli_query($dbc, $payment);
						$row_payment = mysqli_fetch_array($r);

						$query = "INSERT INTO check_in VALUES(NULL,NULL,{$row_guest['id']},'$check_in', '$check_out',{$row_payment['id']},1)";
						mysqli_query($dbc, $query);

						$check_in = "SELECT id FROM check_in WHERE ((guest_id = {$row_guest['id']}) AND (active = 1))";
						$r_checkin = mysqli_query($dbc, $check_in);
						$row_checkin = mysqli_fetch_array($r_checkin);

						if (count($_SESSION['room']) > 0){
							foreach ((array)$_SESSION['room'] as $room_type => $room_type_array){
								foreach((array)$room_type_array as $room => $room_data){
									$room_query = "UPDATE rooms set available = 0, check_in_id = {$row_checkin['id']} WHERE id = $room_data";
									mysqli_query($dbc, $room_query);
								}
							}
						}
						header('Location:../reservation/reservation_manage.php');
				}	else if (count($errors) > 0){
					foreach ($errors as $error){
						echo '<p style="color: red">'.$error.'</p>';
					}
				}
			}	

				print "<div style=\"margin-top: 10px;\">
				<form action=\"cin_payment.php\" method=\"post\">
				<center><h1>Payment</h1></center>
				<div style=\"margin-top: 10px;\">
				<p>Guest name: {$_SESSION['guest'][0]} {$_SESSION['guest'][1]} {$_SESSION['guest'][2]}</p>
				<p>Check in: {$_SESSION['date'][0]}</p>
				<p>Check out: {$_SESSION['date'][1]}</p>
				<p>discount %: <input type=\"text\" name=\"discount\" size=\"10\" placeholder=\"discount %\"/></p>
				<p>total: {$_SESSION['total']} $</p>
				<p>cash in: <input type=\"text\" name=\"cash_in\" size=\"20\" placeholder=\"cash in $\"/></p>
				<p>cash out: <input type=\"text\" name=\"cash_out\" size=\"20\" placeholder=\"cash out $\"/></p>
				<p><input type=\"submit\" name=\"submit\" value=\"Submit\" /></p>
			</form>";
	
	mysqli_close($dbc);
	include '../include/footer.html';
} else {
	header('Location:../login/manager_login.php');
}

?>