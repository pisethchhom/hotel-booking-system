<?php

/********************************************************************
 * 
 * Allow to choose room that available 
 * => The amount of room to choose is base
 * => Room id will transfer once submited 
 * 
 *********************************************************************/

	define('TITLE', 'Choose room');
	session_start();
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) {

if (isset($_POST['submit'])) {
		$error = array();
	
		// check if array has duplicate value
		if (isset($_POST['rt1'])){
			if (count($_POST['rt1']) !== count(array_unique($_POST['rt1']))){
				array_push($error, 'Room type 1 are the same!');
			}
		}
		if (isset($_POST['rt2'])){
			if (count($_POST['rt2']) !== count(array_unique($_POST['rt2']))){
				array_push($error, 'Room type 2 are the same!');
			}
		}
		if (isset($_POST['rt3'])){
			if (count($_POST['rt3']) !== count(array_unique($_POST['rt3']))){
				array_push($error, 'Room type 3 are the same!');
			}
		}
		if (isset($_POST['rt4'])){
			if (count($_POST['rt4']) !== count(array_unique($_POST['rt4']))){
				array_push($error, 'Room type 4 are the same!');
			}
		}
		
		// else create session and move to payment.php
		if (count($error) > 0){
			foreach($error as $err){
				echo '<p style="color: red">'.$err.'</p>';
			}
		} else {
			$_SESSION['room'] = array($_POST['rt1'], $_POST['rt2'], $_POST['rt3'], $_POST['rt4']);
			header('Location: cin_payment.php');
		}
	}
		print '<center><h1>Choose room</h1></center>
		<div style="margin-top: 20px;">';
		print '<form action=\'cin_choose_room.php\' method=\'post\'>
				<table width="800px;">
				<tr>
					<th>Type</th>
					<th>Average Capacity</th>
					<th>Room Number</th>
					<th>Price per room</th>
				</tr>
				';
			
			/****0000************************************************************
			 * 
			 * Check if customer choose room type 1
			 * => if choose, print option for room base on amount 
			 * => else don't print this form 
			 * 
			 *********************************************************************/

			if (isset($_SESSION['roomType']) && ($_SESSION['roomType'][0] > 0)){
				$rt1 = 'SELECT rt.id, rt.name, rt.capacity, rt.price, c.symbol FROM room_type rt JOIN currency_type c 
				ON rt.currency_type_id = c.id WHERE ((rt.id = 1) AND (rt.active = 1))';

				$r_rt1 = mysqli_query($dbc, $rt1);
				$row_rt1 = mysqli_fetch_array($r_rt1);

				for ($i=0; $i<$_SESSION['roomType'][0]; $i++) {
					print "<tr>
									 <td>{$row_rt1['name']}</td>
									 <td>{$row_rt1['capacity']}</td>
									 <td><select name='rt1[]'>";

				$room_q = "SELECT id, rooms_no FROM rooms WHERE ((room_type_id = ".$row_rt1['id'].") AND (available = 1) AND (active = 1))";
				$r_room = mysqli_query($dbc, $room_q);

				while($row_room = mysqli_fetch_array($r_room)){
					print "<option value={$row_room['id']}>{$row_room['rooms_no']}</option>";
				}
					print	"</select></td>
									 <td>{$row_rt1['price']} {$row_rt1['symbol']}</td>
								 </tr>";
				}
			} // end of check customer choose room type 1

			/****0000****0000****************************************************
			 * 
			 * Check if customer choose room type 2
			 * => if choose, print option for room base on amount 
			 * => else don't print this form 
			 * 
			 *********************************************************************/

			if (isset($_SESSION['roomType']) && ($_SESSION['roomType'][1] > 0)){
				$rt2 = 'SELECT rt.id, rt.name, rt.capacity, rt.price, c.symbol FROM room_type rt JOIN currency_type c 
				ON rt.currency_type_id = c.id WHERE ((rt.id = 2) AND (rt.active = 1))';

				$r_rt2 = mysqli_query($dbc, $rt2);
				$row_rt2 = mysqli_fetch_array($r_rt2);

				for ($i=0; $i<$_SESSION['roomType'][1]; $i++) {
					print "<tr>
									 <td>{$row_rt2['name']}</td>
									 <td>{$row_rt2['capacity']}</td>
									 <td><select name='rt2[]'>";

				$room_q = "SELECT id, rooms_no FROM rooms WHERE ((room_type_id = ".$row_rt2['id'].") AND (available = 1) AND (active = 1))";
				$r_room = mysqli_query($dbc, $room_q);

				while($row_room = mysqli_fetch_array($r_room)){
					print "<option value={$row_room['id']}>{$row_room['rooms_no']}</option>";
				}
					print	"</select></td>
									 <td>{$row_rt2['price']} {$row_rt2['symbol']}</td>
								 </tr>";
				}
			} // end of check if customer choose room type 2 
	
			/****0000****0000****0000********************************************
			 * 
			 * Check if customer choose room type 3
			 * => if choose, print option for room base on amount 
			 * => else don't print this form 
			 * 
			 *********************************************************************/

			if (isset($_SESSION['roomType']) && ($_SESSION['roomType'][2] > 0)){
				$rt3 = 'SELECT rt.id, rt.name, rt.capacity, rt.price, c.symbol FROM room_type rt JOIN currency_type c 
				ON rt.currency_type_id = c.id WHERE ((rt.id = 3) AND (rt.active = 1))';

				$r_rt3 = mysqli_query($dbc, $rt3);
				$row_rt3 = mysqli_fetch_array($r_rt3);

				for ($i=0; $i<$_SESSION['roomType'][2]; $i++) {
					print "<tr>
									 <td>{$row_rt3['name']}</td>
									 <td>{$row_rt3['capacity']}</td>
									 <td><select name='rt3[]'>";

				$room_q = "SELECT id, rooms_no FROM rooms WHERE ((room_type_id = ".$row_rt3['id'].") AND (available = 1) AND (active = 1))";
				$r_room = mysqli_query($dbc, $room_q);

				while($row_room = mysqli_fetch_array($r_room)){
					print "<option value={$row_room['id']}>{$row_room['rooms_no']}</option>";
				}
					print	"</select></td>
									 <td>{$row_rt3['price']} {$row_rt3['symbol']}</td>
								 </tr>";
				}
			} // end of check if customer choose room type 3 

			/****0000****0000****0000****0000************************************
			 * 
			 * Check if customer choose room type 4
			 * => if choose, print option for room base on amount 
			 * => else don't print this form 
			 * 
			 *********************************************************************/

			if (isset($_SESSION['roomType']) && ($_SESSION['roomType'][3] > 0)){
				$rt4 = 'SELECT rt.id, rt.name, rt.capacity, rt.price, c.symbol FROM room_type rt JOIN currency_type c 
				ON rt.currency_type_id = c.id WHERE ((rt.id = 4) AND (rt.active = 1))';

				$r_rt4 = mysqli_query($dbc, $rt4);
				$row_rt4 = mysqli_fetch_array($r_rt4);

				for ($i=0; $i<$_SESSION['roomType'][3]; $i++) {
					print "<tr>
									 <td>{$row_rt4['name']}</td>
									 <td>{$row_rt4['capacity']}</td>
									 <td><select name='rt4[]'>";

				$room_q = "SELECT id, rooms_no FROM rooms WHERE ((room_type_id = ".$row_rt4['id'].") AND (available = 1) AND (active = 1))";
				$r_room = mysqli_query($dbc, $room_q);

				while($row_room = mysqli_fetch_array($r_room)){
					print "<option value={$row_room['id']}>{$row_room['rooms_no']}</option>";
				}
					print	"</select></td>
									 <td>{$row_rt4['price']} {$row_rt4['symbol']}</td>
								 </tr>";
				}
			} // end of check if customer choose room type 4 

			print '</table><input type=\'submit\' name=\'submit\' value=\'next\'/></form>';  
    mysqli_close($dbc);
    include '../include/footer.html';
} else {
	header('Location:../login/manager_login.php');
}

?>