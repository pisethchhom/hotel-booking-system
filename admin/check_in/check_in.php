<?php
	
	define('TITLE', 'Check in');
	session_start();
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) {

if (isset($_SESSION['reservation'])) {
	unset($_SESSION['reservation']);
}

if (isset($_SESSION['guest'])){
	unset($_SESSION['guest']);
}

if (isset($_SESSION['total'])){
	unset($_SESSION['total']);
}

if (isset($_SESSION['date'])) {
	unset($_SESSION['date']);
}

// delete session room
if (isset($_SESSION['room'])){
	unset($_SESSION['room']);
}

if (isset($_SESSION['roomType'])){
	unset($_SESSION['roomType']);
}

// allow to choose reservation
if(isset($_POST['booked'])) {
	header('Location: cin_booked_cus.php');
} 

/********************************************************************
 * 
 * Choosen not booked customer  
 * => Need to store info of customer 
 * => Fill in the form of check-in
 * 
 *********************************************************************/

elseif (isset($_POST['not_booked'])){
	$_SESSION['guest'] = true;
	header('Location: ../guest/add_guest.php');
} 
 
 /********************************************************************
 * 
 * Print the option of choosing booked or not booked customer 
 * => if booked, move to $_POST['booked'] form
 * => else, move to $_POST['not_booked'] form
 * 
 *********************************************************************/
 
 else {
		print '<form action="check_in.php" method="post">
					<center><h1>Check in!</h1></center>
					<div>
						<p><input type="submit" name="booked" value="Booked Customer" /></p>
					</div>
					<div>
						<p><input type="submit" name="not_booked" value="Not Booked Customer" /></p>
					</div>
				</form>
	';
	} // end of choose old or new customer

	mysqli_close($dbc);
	include '../include/footer.html';

} else {
	header('Location: ../login/manager_login.php');
}
?>