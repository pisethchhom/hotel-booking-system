<?php 

$page_title = 'View Reservations';
session_start();

include ('../include/manager_header.html');
require '../include/mysqli_connect.php';

?>
</br>
<h1>Reservations</h1>
<?php 
if (isset($_SESSION['admin_name'])) {

include '../system/timetable.php';

print '<div style="margin-top: 10px;">
<table width="1000px;">
		<tr>
			<th>Number</th>
			<th>ID</th>
			<th>Made by</th>
			<th>Guest Name</th>
			<th>STANDARD SINGLE</th>
			<th>DELUXE DOUBLE</th>
			<th>SUPERIOR TWIN</th>
			<th>FAMILY SUITE</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Comment</th>
			<th>Total</th>
		</tr>
		';

$reservation_query = 'SELECT re.id, re.add_by, re.rt1, re.rt2, re.rt3, re.rt4, re.in_date,
re.out_date, re.total, re.special_offer, g.id as gid, g.title, g.fname, g.lname, c.symbol 
FROM reservation re JOIN guest g ON re.guest_id = g.id JOIN currency_type c ON re.currency_type_id = c.id WHERE re.active = 1 ORDER BY re.id';
if ($r = mysqli_query($dbc, $reservation_query)) { 

	$count = 0;
	while ($reservation_row = mysqli_fetch_array($r)) {
		if (count($reservation_row) > 0){
			$count++;
			echo "<tr>
					<td>$count</td>
					<td><a href=\"../invoice/reservation_info.php?id={$reservation_row['id']}\">{$reservation_row['id']}</a></td>
					<td>{$reservation_row['add_by']}</td>
					<td><a href=\"../guest/check_guest.php?id={$reservation_row['gid']}\">{$reservation_row['title']} {$reservation_row['fname']} {$reservation_row['lname']}</a></td>
					<td>{$reservation_row['rt1']}</td>
					<td>{$reservation_row['rt2']}</td>
					<td>{$reservation_row['rt3']}</td>
					<td>{$reservation_row['rt4']}</td>
					<td>{$reservation_row['in_date']}</td>
					<td>{$reservation_row['out_date']}</td>
					<td>{$reservation_row['special_offer']}</td>
					<td>{$reservation_row['total']} {$reservation_row['symbol']}</td>
					<td><a href=\"delete_reservation.php?id={$reservation_row['id']}\">Delete</a></td>
			</tr>";
		}
	}	
print '</table>'; 

if ($count == 0){
			echo '<p style="color: red;">There is no data right now!</p>';
}
	print '<div style="margin-top: 100px;">';	
} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $reservation_query . '</p>';
} 


mysqli_close($dbc); 
include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>