<?php 

$page_title = 'View Rooms';
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';

?>
</br>
<h1>Rooms</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
print '<div style="margin-top: 10px;">';
print '<a href="add_room.php">Add Room</a>
<div style="margin-top: 10px;">
<table width="800px;">
		<tr>
			<th>Number</th>
			<th>Room No.</th>
			<th>Floor No.</th>
			<th>Room Type</th>
			<th>Capacity</th>
			<th>Type Detail</th>
			<th>Description</th>
			<th>State</th>
			<th>Price</th>
		</tr>
		';

$query = 'SELECT r.id, r.rooms_no, r.floor_no, r.description, r.available, rt.name, rt.capacity, 
rt.info, rt.currency_type_id, rt.price, rt.active FROM rooms r JOIN room_type rt ON r.room_type_id = rt.id 
WHERE r.active = 1 ORDER BY r.id';

if ($r = mysqli_query($dbc, $query)) { 
	$count = 0;

	while ($row = mysqli_fetch_array($r)) {
		$symbol = "SELECT symbol FROM currency_type WHERE (id = {$row['currency_type_id']} AND active = 1)";
		$r_symbol = mysqli_query($dbc, $symbol);
		$row_symbol = mysqli_fetch_assoc($r_symbol);
		$count++;

		echo "<tr>
				<td>$count</td>
				<td>{$row['rooms_no']}</td>
				<td>{$row['floor_no']}</td>
				<td>";
		if ($row['active'] == 0){
			echo "This room type is not available.</td>
				<td>---</td>
				<td>---</td>
				<td>---</td>	
				<td>---</td>
				<td><a href=\"edit_room.php?id={$row['id']}\">Edit</a>
				<a href=\"delete_room.php?id={$row['id']}\">Delete</a></td>
		</tr>";
		} else {
			echo $row['name']."</td>
				<td>{$row['capacity']}</td>
				<td>{$row['info']}</td>
				<td>{$row['description']}</td>";

		if ($row['available'] == 1){
			echo "<td>Available</td>";

		} else {
			echo "<td>Booked</td>";
		}
			echo "<td>{$row['price']}"."{$row_symbol['symbol']}</td>
				<td><a href=\"edit_room.php?id={$row['id']}\">Edit</a>
				<a href=\"delete_room.php?id={$row['id']}\">Delete</a></td>
		</tr>";
		}
	}	
} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $query . '</p>';
} 
print '</table>'; 
print '<div style="margin-top: 100px;">';

mysqli_close($dbc); 
include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>