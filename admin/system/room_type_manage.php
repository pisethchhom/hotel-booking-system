<?php 

$page_title = 'View Room Type';
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';

?>
</br>
<h1>Room Types	</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
print '<div style="margin-top: 10px;">';
print '<a href="add_room_type.php">Add room type</a>
<div style="margin-top: 10px;">
<table width="800px;">
		<tr>
			<th>Number</th>
			<th>Name</th>
			<th>Capacity</th>
			<th>Information</th>
			<th>Price</th>
		</tr>
		';

$query = 'SELECT rt.id, rt.name, rt.capacity, rt.info, c.type, c.active as c_active, 
rt.price, c.symbol FROM room_type rt JOIN currency_type c ON rt.currency_type_id = c.id WHERE rt.active = 1 ORDER BY rt.id' ;
if ($r = mysqli_query($dbc, $query)) { 
	$count = 0;

	while ($row = mysqli_fetch_array($r)) {
		$count++;

		echo "<tr>
				<td>$count</td>
				<td>{$row['name']}</td>
				<td>{$row['capacity']}</td>
				<td>{$row['info']}</td>
				<td>";
		if ($row['c_active'] == 0) {
			echo "The currency type is not available.</td>
				<td><a href=\"edit_room_type.php?id={$row['id']}\">Edit</a>
				<a href=\"delete_room_type.php?id={$row['id']}\">Delete</a></td>
		</tr>";
		} else {
			echo $row['price']." ".$row['symbol']."</td>
				<td><a href=\"edit_room_type.php?id={$row['id']}\">Edit</a>
				<a href=\"delete_room_type.php?id={$row['id']}\">Delete</a></td>
		</tr>";
			}
	}	
} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $query . '</p>';
} 
print '</table>'; 
print '<div style="margin-top: 100px;">';

mysqli_close($dbc); 
include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>