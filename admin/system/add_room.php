<?php
	
	define('TITLE', 'Add Room');
	session_start(); // start session
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) { // check if logged in
	if (isset($_POST['Submit'])) {

		// convert to string
		$room_no = mysqli_real_escape_string($dbc, $_POST['room_no']);
		$floor_no = mysqli_real_escape_string($dbc, $_POST['floor_no']);
		$room_type = mysqli_real_escape_string($dbc, $_POST['room_type']);
		$description = mysqli_real_escape_string($dbc, $_POST['description']);
		
		$errors = array();

		// check if form is filled correctly
		if (empty($room_no)) {array_push($errors, 'Room No. is required!');}
		if (empty($floor_no)) {array_push($errors, 'Floor No. is required!');}
		if (empty($room_type)) {array_push($errors, 'Room type is required!');}

		if (count($errors) == 0) {

			$room = "SELECT * FROM rooms WHERE rooms_no = '$room_no'"; // check if room already added
			$r_room = mysqli_query($dbc, $room);
			$row_room = mysqli_fetch_assoc($r_room);

			if ($row_room['active'] == 1) { // if room already added (active = 1)
				array_push($errors, 'Room is already added!');

			} else {
				$roomtype = "SELECT * FROM room_type WHERE (name = '$room_type' AND active = 1)";
				$r_rt = mysqli_query($dbc, $roomtype);

				if ($row_rt = mysqli_fetch_assoc($r_rt)){ // if room type is exist (active = 1)
					if ($row_room) { // if room is disable (active = 0) UPDATE
						$query = "UPDATE rooms SET floor_no = '$floor_no', room_type_id = {$row_rt['id']}, 
						description = '$description',available = 1, active = 1 WHERE id = {$row_room['id']}";

					} else { // if room not exist (!$row_room) INSERT
						$query = "INSERT INTO rooms VALUES(NULL,NULL,'$room_no', '$floor_no', {$row_rt['id']},'$description',1,1)";
					}	
					$r = mysqli_query($dbc, $query);
					header('Location: room_manage.php');
	
				} else { // if room type is disable (active = 0)
					array_push($errors, 'Room type does not exist.');
				} 
			} 
		}
		if (count($errors) > 0){ // print error
			foreach ($errors as $error){
				echo '<p style="color: red">'.$error.'</p>';
				}
			} // end of print error	
	}
} else { // if not loggedin
	header('Location: ../login/manager_login.php');
}
?> <!-- form for inserting room -->
	<form action="add_room.php" method="post">
		<center><h1>Add Room</h1></center>
		<p>Room No.<br /><input type="text" name="room_no" size="5" placeholder="Room No."/></p>
		<p>Floor No.<br /><input type="text" name="floor_no" size="10" placeholder="Floor No."/></p>
		<p>Room type<br /><input type="text" name="room_type" size="20" placeholder="Room type"/></p>
		<p>Description<br /><input type="text" name="description" placeholder="Description"/></p>
		<p><input type="submit" name="Submit" value="Submit" /></p>
	</form>

<?php
	mysqli_close($dbc);
	include '../include/footer.html';
?>