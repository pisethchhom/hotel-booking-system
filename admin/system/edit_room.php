<?php 

$page_title = 'Edit a Room';
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';
?>
</br>
<h1>Edit a Room</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
	if (isset($_GET['id']) && is_numeric($_GET['id']) ) { 
		$query = "SELECT r.rooms_no, r.floor_no, rt.name, r.description, r.available
		FROM rooms r JOIN room_type rt ON r.room_type_id = rt.id WHERE (r.id={$_GET['id']} AND r.active = 1)";

		if ($r = mysqli_query($dbc, $query)) { 
			$row = mysqli_fetch_assoc($r); 
			if ($row['available'] == 1){
				$room_avaliable = "Available";
			} else {
				$room_avaliable = "Booked";
			}
			print '<form action="edit_room.php" method="post">
		<p>Room No.: <input type="text" name="room_no" size="5" value="' 
		. htmlentities($row['rooms_no']) . '"></p>
		<p>Floor No.: <input type="text" name="floor_no" size="5" value="' 
		. htmlentities($row['floor_no']) . '"></p>
		<p>Room Type:<input type="text" name="room_type" size="20" value="' 
		. htmlentities($row['name']) . '"</p>
		<p>Description:<input type="text" name="description" value="' 
		. htmlentities($row['description']) . '"</p>
		<p>State:<input type="text" name="description" value = '.$room_avaliable.' </p>

		<input type="hidden" name="id" value="' . $_GET['id'] . '">
		<br/>
		<input type="submit" name="submit" value="Update Room">
		</form>';

		} else { 
			print '<p style="color: red;">Could not retrieve because:<br>' . mysqli_error($dbc) 
			. '.</p><p>The query being run was: ' . $query . '</p>';
		}
		
	} elseif (isset($_POST['id']) && is_numeric($_POST['id'])) { 
		$roomtype = "SELECT * FROM room_type WHERE name = '{$_POST['room_type']}'";
		$r_rt = mysqli_query($dbc, $roomtype);
		$problem = FALSE;

		if (!($row_rt = mysqli_fetch_assoc($r_rt))) {
			print '<p style="color: red;">Room type does not exist.</p>';
			$problem = TRUE;
		}elseif (!empty($_POST['room_no']) && !empty($_POST['room_type']) && !empty($_POST['floor_no'])) {
			$room_no = mysqli_real_escape_string($dbc, trim(strip_tags($_POST['room_no'])));
			$room_type = $row_rt['id'];
			$floor_no = mysqli_real_escape_string($dbc, trim(strip_tags($_POST['floor_no'])));
			$description = mysqli_real_escape_string($dbc, trim(strip_tags($_POST['description'])));
		} else {
			print '<p style="color: red;">Please submit every information.</p>';
			$problem = TRUE;
		} 
		if (!$problem) {
			
			$query = "UPDATE rooms SET rooms_no='$room_no', floor_no='$floor_no', description='$description', room_type_id=$room_type WHERE id={$_POST['id']}";
			$r = mysqli_query($dbc, $query); 
			if (mysqli_affected_rows($dbc) == 1) {
				print '<p>The room has been updated.</p>';
			} else {
				print '<p style="color: red;">Could not update the room because:<br>' . mysqli_error($dbc) 
				. '.</p><p>The query being run was: ' . $query . '</p>';
			}
		} 
	} else { 
		print '<p style="color: red;">This page has been accessed in error.</p>';
	} 
	mysqli_close($dbc); 
	include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>