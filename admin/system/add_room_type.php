<?php
	
	define('TITLE', 'Add Room Type');
	session_start(); // start session
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) { // check if logged in
	if (isset($_POST['Submit'])) {
		// type_name 
		// capacity
		// info
		// price
		// currency_type

		// convert to string
		$type_name = mysqli_real_escape_string($dbc, $_POST['type_name']);
		$capacity = (int)$_POST['capacity'];
		$info = mysqli_real_escape_string($dbc, $_POST['info']);
		$price = (int)$_POST['price'];
		$currency_type = (int)$_POST['currency_type'];
		
		$errors = array();

		// check if form is filled correctly
		if (empty($type_name)) {array_push($errors, 'Type name is required!');}
		if (empty($capacity)) {array_push($errors, 'Capacity is required!');}
		if (empty($price)) {array_push($errors, 'Price is required!');}
		if (empty($currency_type)) {array_push($errors, 'Currency type is required!');}

		if (count($errors) == 0) {

			$type = "SELECT * FROM room_type WHERE name = '$type_name'"; // check if room type already added
			$r_type = mysqli_query($dbc, $type);
			$row_type = mysqli_fetch_assoc($r_type);

			if ($row_type['active'] == 1) { // if room type already added (active = 1)
				array_push($errors, 'Room type is already added!');
				
			} else{
				if ($row_type){ // if active = 0
					$rt = "UPDATE room_type SET capacity = $capacity, info = '$info', currency_type_id = $currency_type, price = $price, active = 1 WHERE id=".$row_type['id']."";

				} else { // if not added
					$rt = "INSERT INTO room_type VALUES (NULL,NULL,'$type_name',$capacity,'$info',$currency_type,$price,1)";
				}
				if ($r = mysqli_query($dbc, $rt)){
					echo $rt;
					//header('Location: room_type_manage.php');

				} else { 
					print '<p style="color: red;">Could not retrieve because:<br>' . mysqli_error($dbc) 
					. '.</p><p>The query being run was: ' . $query . '</p>';
				}
			}
		}
		if (count($errors) > 0){ // print error
			foreach ($errors as $error){
				echo '<p style="color: red">'.$error.'</p>';
				}
			} // end of print error	
	}
} else { // if not loggedin
	header('Location: ../manager_login.php');
}
?> <!-- form for inserting room -->
	<form action="add_room_type.php" method="post">
		<center><h1>Add Room Type</h1></center>
		<p>Type Name<br /><input type="text" name="type_name" size="20" placeholder="Type Name"/></p>
		<p>Capacity<br /><input type="text" name="capacity" size="10" placeholder="Capacity"/></p>
		<p>Information<br /><input type="text" name="information" placeholder="Information"/></p>
		<p>Price<br /><input type="text" name="price" placeholder="Price"/></p>
		<?php
			$ct = 'SELECT id, symbol FROM currency_type WHERE active = 1 ORDER BY id';

			if ($cr = mysqli_query($dbc, $ct)) {
			print '<p>Currency Type: <select name="currency_type">
			<option value="">-- select one --</option>';
	
				while ($row_c = mysqli_fetch_array($cr)) {
				echo '<option value='.$row_c['id'].'>'.$row_c['symbol'].'</option>';
				}
			} else {
				print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
		<p>The query being run was: ' . $query . '</p>';
			}
		?>
		</select></p>
		<p><input type="submit" name="Submit" value="Submit" /></p>
	</form>

<?php
	mysqli_close($dbc);
	include '../include/footer.html';
?>