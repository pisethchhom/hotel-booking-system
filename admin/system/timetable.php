<?php
require '../include/mysqli_connect.php';

/***********************************************
 * 
 * set after date
 * => select id of today from time table 
 * => set the day before id of today active to 0
 * 
 ***********************************************/
$today = 'SELECT id from timetable where((month_id = '.date("m").') AND (date_num = '.date("d").') AND (active = 1))';
$r = mysqli_query($dbc, $today);
$row_today = mysqli_fetch_array($r);
$before_date = "SELECT id from timetable where((id < {$row_today['id']} ) AND (active = 1))";
$bd = mysqli_query($dbc, $before_date);

if ($row = mysqli_fetch_array($bd)){
  $update = 'UPDATE timetable set active = 0 where((id < '.$row_today['id'].' ) AND (active = 1))';
  mysqli_query($dbc, $update);

}

/************************************************
 * 
 * delete time table
 * => select data that active = 0, and no rt value (no booking)
 * => delete those data from table 
 * 
 ***********************************************/
$today = date("d");
$select_before_today = 'SELECT * from timetable where ((rt1 = 0) AND (rt2 = 0) AND (rt3 = 0) AND (rt4 = 0) AND (active = 0))';

if ($r = mysqli_query($dbc, $select_before_today)) {
  if(mysqli_fetch_array($r) == 0){
  } else {
      $delete = 'DELETE from timetable where ((rt1 = 0) AND (rt2 = 0) AND (rt3 = 0) AND (rt4 = 0) AND (active = 0))';
      mysqli_query($dbc, $delete);
  }
} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $select_before_today . '</p>';
}

/************************************************
 * 
 * add time table
 * => compare if next month id data is create
 * => if active is 0, means its last year month
 * => if active is 1, means its created already
 * 
 ***********************************************/
$next_month = date("m") + 1;
$select_timetable = 'SELECT * from timetable where ((month_id = '.$next_month.') AND (rt1 = 0) AND (rt2 = 0) AND (rt3 = 0) AND (rt4 = 0) AND (active = 1))';

if ($r = mysqli_query($dbc, $select_timetable)) {
  if(mysqli_fetch_array($r) == 0){
    $month = 'SELECT * from month where id = '.$next_month.'';

    if ($m = mysqli_query($dbc, $month)){
      $row = mysqli_fetch_array($m);

      for ($i = 1; $i <= $row['date_limit']; $i++) {
        $add_date = "INSERT INTO timetable VALUES(NULL, $next_month, $i,0,0,0,0,1)";
        mysqli_query($dbc, $add_date);
      }
    }else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $month . '</p>';
}
  }
} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $select_timetable . '</p>';
}
?>