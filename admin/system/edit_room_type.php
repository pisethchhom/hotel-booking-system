<?php 

$page_title = 'Edit a Room Type';
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';
?>
</br>
<h1>Edit a Room Type</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
	if (isset($_GET['id']) && is_numeric($_GET['id']) ) { 
		$query = 'SELECT rt.id, rt.name, rt.capacity, rt.info, c.type, c.active as c_active, 
		rt.price, c.symbol FROM room_type rt JOIN currency_type c ON rt.currency_type_id = c.id WHERE rt.id = '.$_GET['id'].'' ;

		if ($r = mysqli_query($dbc, $query)) { 
			$row = mysqli_fetch_assoc($r); 
			if ($row['c_active'] == 0){
				$currency_type = "Not Available";
			}
		
			print '<form action="edit_room_type.php" method="post">
		<p>Type Name: <input type="text" name="type_name" size="20" value="' 
		. htmlentities($row['name']) . '"></p>
		<p>Capacity: <input type="text" name="capacity" size="10" value="' 
		. htmlentities($row['capacity']) . '"></p>
		<p>Information:<input type="text" name="information" value="' 
		. htmlentities($row['info']) . '"</p>
		<p>Price:<input type="text" name="price" value="' 
		. htmlentities($row['price']) . '"</p>';

		$ct = 'SELECT id, symbol FROM currency_type WHERE active = 1 ORDER BY id';

		if ($cr = mysqli_query($dbc, $ct)) {
		print '<p>Currency Type: <select name="currency_type">
		<option value="">-- select one --</option>';

			while ($row_c = mysqli_fetch_array($cr)) {
			echo '<option value='.$row_c['id'].'>'.$row_c['symbol'].'</option>';
			}
			print '</select>		
			<input type="hidden" name="id" value="' . $_GET['id'] . '">
			<br/>
			<br/>
			<input type="submit" name="submit" value="Update Room Type">
			</form>';
		} else {
			print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $query . '</p>';
		}

		} else { 
			print '<p style="color: red;">Could not retrieve because:<br>' . mysqli_error($dbc) 
			. '.</p><p>The query being run was: ' . $query . '</p>';
		}
	} elseif (isset($_POST['id']) && is_numeric($_POST['id'])) { 
		$problem = FALSE;

		if (!empty($_POST['type_name']) && !empty($_POST['capacity']) && !empty($_POST['information']) && !empty($_POST['price']) && !empty($_POST['currency_type'])) {
			$room_type = mysqli_real_escape_string($dbc, trim(strip_tags($_POST['type_name'])));
			$capacity = (int)$_POST['capacity'];
			$price = (int)$_POST['price'];
			$currency_type = $_POST['currency_type'];
			$information = mysqli_real_escape_string($dbc, trim(strip_tags($_POST['information'])));
		} else {
			print '<p style="color: red;">Please submit every information.</p>';
			$problem = TRUE;
		} 
		if (!$problem) {
			
			$query = "UPDATE room_type SET name='$room_type', capacity=".$capacity.", info='$information', price=$price, currency_type_id=$currency_type WHERE id={$_POST['id']}";
			$r = mysqli_query($dbc, $query); 
			if (mysqli_affected_rows($dbc) == 1) {
				print '<p>Room type has been updated.</p>';
			} else {
				print '<p style="color: red;">Could not update the room type because:<br>' . mysqli_error($dbc) 
				. '.</p><p>The query being run was: ' . $query . '</p>';
			}
		} 
	} else { 
		print '<p style="color: red;">This page has been accessed in error.</p>';
	} 
	mysqli_close($dbc); 
	include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>