<?php 

$page_title = 'Delete a Room Type';
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';
?>
</br>
<h1>Delete a Room Type</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
	if (isset($_GET['id']) && is_numeric($_GET['id']) ) { 

		$query = "SELECT * from room_type WHERE id={$_GET['id']}";
		if ($r = mysqli_query($dbc, $query)) { // Run the query.
			// Make the form:
			print '<form action="delete_room_type.php" method="post">
			<p>Are you sure you want to delete this room?</p>

			<input type="hidden" name="id" value="' . $_GET['id'] . '">
			<input type="submit" name="submit" value="Delete this Room Type!"></p>
			</form>';

		} else { // Couldn't get the information.
			print '<p style="color: red;">Could not retrieve because:<br>' 
			. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
		}

	} elseif (isset($_POST['id']) && is_numeric($_POST['id'])) { // Handle the form.

		// Delete from table
		$query = "UPDATE room_type SET active = 0 WHERE id={$_POST['id']}";
		$r = mysqli_query($dbc, $query); // Execute the query.
		// Report on the result:
		if (mysqli_affected_rows($dbc) == 1) {
			print '<p>The room type has been deleted.</p>';
		} else {
			print '<p style="color: red;">Could not delete the room type because:<br>' 
			. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
		}
	} else { // No ID received.
		print '<p style="color: red;">This page has been accessed in error.</p>';
	} // End of main IF.
	mysqli_close($dbc); // Close the connection.
	include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>