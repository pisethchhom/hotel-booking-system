<?php
require('fpdf17/fpdf.php');
session_start();
require '../include/mysqli_connect.php';

if (isset($_SESSION['admin_name'])) {

	$q = mysqli_query($dbc,"SELECT re.id, re.add_date, re.total, re.rt1, re.rt2, re.rt3, re.rt4, re.in_date, re.out_date, 
	g.title, g.fname, g.lname, g.email, g.phone_no, re.guest_id, g.passport_no, g.nationality 
	FROM reservation re JOIN guest g ON re.guest_id = g.id WHERE re.id = {$_GET['id']}");

	$query = mysqli_fetch_array($q);
	$r = mysqli_query($dbc,'SELECT price from room_type order by id');
	//A4 width : 219mm
	//default margin : 10mm each side
	//writable horizontal : 219-(10*2)=189mm

	$pdf = new FPDF('P','mm','A4');

	$pdf->AddPage();

	//set font to arial, bold, 14pt
	$pdf->SetFont('Arial','B',14);

	//Cell(width , height , text , border , end line , [align] )

	$pdf->Cell(130	,5,'iHotel',0,0);

	$pdf->SetFont('Arial','B',20);
	$pdf->Cell(59	,5,'Reservation Info',0,1);//end of line

	//set font to arial, regular, 12pt
	$pdf->SetFont('Arial','',10);

	$pdf->Cell(130	,5,'Street: st.999',0,0);
	$pdf->Cell(59	,5,'',0,1);//end of line

	$pdf->Cell(130	,5,'Phnom Penh, Cambodia',0,0);
	$pdf->Cell(25	,5,'Date',0,0);
	$add_date = new DateTime($query['add_date']);
	$pdf->Cell(34	,5,$add_date->format('D-d/M/Y'),0,1);//end of line

	$pdf->Cell(130	,5,'Phone [+85512345678]',0,0);

	$pdf->Cell(25	,5,'Invoice #',0,0);
	$pdf->Cell(34	,5,$query['id'],0,1);//end of line


	$pdf->Cell(25	,5,'Customer ID #',0,0);
	$pdf->Cell(34	,5,$query['guest_id'],0,1);//end of line

	//make a dummy empty cell as a vertical spacer
	$pdf->Cell(189	,10,'',0,1);//end of line

	$pdf->SetFont('Arial','B',14);

	//billing address
	$pdf->Cell(100	,5,'Guest',0,1);//end of line

	$pdf->SetFont('Arial','',10);

	//add dummy cell at beginning of each line for indentation
	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,"Name: {$query['title']} {$query['fname']} {$query['lname']}",0,1);

	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,"Nationality: {$query['nationality']}",0,1);

	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,"Email: {$query['email']}",0,1);

	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,"Phone: {$query['phone_no']}",0,1);

	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,"Passport Number: {$query['passport_no']}",0,1);

	$pdf->Cell(10	,5,'',0,0);
	$pdf->Cell(90	,5,'**Please save this pdf file to confirm at the hotel**',0,1);

	//make a dummy empty cell as a vertical spacer
	$pdf->Cell(189	,10,'',0,1);//end of line

	//invoice contents
	$pdf->SetFont('Arial','B',12);

	$pdf->Cell(130	,6,'Description',1,0,'C');
	// $pdf->Cell(25	,5,'Date',1,0);
	$pdf->Cell(59	,6,'Date',1,1,'C');//end of line

	$pdf->SetFont('Arial','',9);

	//Numbers are right-aligned so we give 'R' after new line parameter

	$pdf->Cell(130	,6,'In date',1,0);
	$date_in = new DateTime($query['in_date']);
	$pdf->Cell(59	,6,$date_in->format('D-d/M/Y'),1,1,'R');//end of line

	$pdf->Cell(130	,6,'Out date',1,0);
	$date_out = new DateTime($query['out_date']);
	$pdf->Cell(59	,6,$date_out->format('D-d/M/Y'),1,1,'R');//end of line

	$pdf->Cell(130	,6,'',0,0);
	$pdf->Cell(59	,6,'',0,1);

	$pdf->SetFont('Arial','B',12);

	$pdf->Cell(100	,6,'Room Type',1,0,'C');
	$pdf->Cell(20	,6,'Amount',1,0,'C');
	$pdf->Cell(40	,6,'Price per Room',1,0,'C');
	$pdf->Cell(29	,6,'Price',1,1,'C');//end of line

	$pdf->SetFont('Arial','',9);

	$pdf->Cell(100	,6,'STANDARD SINGLE',1,0);
	$pdf->Cell(20	,6,$query['rt1'],1,0);
	$row = mysqli_fetch_array($r);
	$price_ty1 = $row[0];
	$pdf->Cell(40	,6,$price_ty1.'$',1,0,'R');
	$pdf->Cell(29	,6,(string)($query['rt1'] * $row[0]).'$',1,1,'R');//end of line

	$pdf->Cell(100	,6,'DELUXE DOUBLE ROOM',1,0);
	$pdf->Cell(20	,6,$query['rt2'],1,0);
	$row = mysqli_fetch_array($r);
	$price_ty2 = $row[0];
	$pdf->Cell(40	,6,$price_ty2.'$',1,0,'R');
	$pdf->Cell(29	,6,(string)($query['rt2'] * $row[0]).'$',1,1,'R');//end of line

	$pdf->Cell(100	,6,'SUPERIOR TWIN',1,0);
	$pdf->Cell(20	,6,$query['rt3'],1,0);
	$row = mysqli_fetch_array($r);
	$price_ty3 = $row[0];
	$pdf->Cell(40	,6,$price_ty3.'$',1,0,'R');
	$pdf->Cell(29	,6,(string)($query['rt3'] * $row[0]).'$',1,1,'R');//end of line

	$pdf->Cell(100	,6,'FAMILY SUITE',1,0);
	$pdf->Cell(20	,6,$query['rt4'],1,0);
	$row = mysqli_fetch_array($r);
	$price_ty4 = $row[0];
	$pdf->Cell(40	,6,$price_ty4.'$',1,0,'R');
	$pdf->Cell(29	,6,(string)($query['rt4'] * $row[0]).'$',1,1,'R');//end of line

	//summary
	$pdf->Cell(131	,6,'',0,0);
	$pdf->Cell(25	,6,'Subtotal',0,0,'C');
	$pdf->Cell(4	,6,'',0,0);
	$price = 0;
	$i = 0;
	while($i < 4){
		switch($i){
			case 0: $price += ($query['rt1'] * $price_ty1); break;
			case 1: $price += ($query['rt2'] * $price_ty2); break;
			case 2: $price += ($query['rt3'] * $price_ty3); break;
			case 3: $price += ($query['rt4'] * $price_ty4); break;
		}
		++$i;
	}
	$pdf->Cell(29	,6,$price.'$',1,1,'R');//end of line

	$date_diff = date_diff($date_in, $date_out);

	$pdf->Cell(131	,6,'',0,0);
	$pdf->Cell(25	,6,'Stay',0,0,'C');
	$pdf->Cell(4	,6,'x',1,0);

	if ($date_diff->days > 0){
		$stay = ' days';
	} else {
		$stay = ' day';
	}
	$pdf->Cell(29	,6,$date_diff->days.$stay,1,1,'R');//end of line

	$pdf->Cell(131	,6,'',0,0);
	$pdf->Cell(25	,6,'Total',0,0,'C');
	$pdf->Cell(4	,6,'',0,0);
	$pdf->Cell(29	,6,$query['total'].'$',1,1,'R');//end of line

	// $pdf->Cell(130	,5,'',0,0);
	// $pdf->Cell(25	,5,'Taxable',0,0);
	// $pdf->Cell(4	,5,'$',1,0);
	// $pdf->Cell(30	,5,'0',1,1,'R');//end of line

	// $pdf->Cell(130	,5,'',0,0);
	// $pdf->Cell(25	,5,'Tax Rate',0,0);
	// $pdf->Cell(4	,5,'$',1,0);
	// $pdf->Cell(30	,5,'10%',1,1,'R');//end of line

	// $pdf->Cell(130	,5,'',0,0);
	// $pdf->Cell(25	,5,'Total Due',0,0);
	// $pdf->Cell(4	,5,'$',1,0);
	// $pdf->Cell(30	,5,'4,450',1,1,'R');//end of line

	$pdf->Output();
	mysqli_close($dbc);
} else {
	header('Location: ../login/manager_login.php');
}
?>
