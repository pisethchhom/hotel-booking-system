<?php
	
	define('TITLE', 'Registration');
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

	if (isset($_POST['register'])) {
		$fname = mysqli_real_escape_string($dbc, $_POST['fname']);
		$lname = mysqli_real_escape_string($dbc, $_POST['lname']);
		$email = mysqli_real_escape_string($dbc, $_POST['email']);
		$phone = mysqli_real_escape_string($dbc, $_POST['phone']);
		$password = mysqli_real_escape_string($dbc, $_POST['password']);
		$confirmpassword = mysqli_real_escape_string($dbc, $_POST['confirmpassword']);

		$errors = array();
		if (empty($fname)) {array_push($errors, 'First name is required!');}
		if (empty($lname)) {array_push($errors, 'Last name is required!');}
		if (empty($email)) {array_push($errors, 'Email is required!');}
		if (empty($phone)) {array_push($errors, 'Phone number is required!');}
		if (empty($password)) {array_push($errors, 'Password is required!');}
		if ($password != $confirmpassword) {array_push($errors, 'Passwords not match!');}

		$query = 'SELECT * FROM managers WHERE email = "$email"';
		$r = mysqli_query($dbc, $query);
		$user = mysqli_fetch_assoc($r);

		if ($user['email'] === $email) {
			array_push($errors, 'Email already exists!');
		}
		
		if (count($errors) == 0) {
			$pw = md5($password);
			$query = "INSERT INTO managers (fname, lname, email, phone_no, pw, register_date, active) VALUES('$fname', '$lname', '$email', '$phone', '$pw', NOW(), 1)";
			mysqli_query($dbc, $query);
			header('Location: manager_login.php');
		} else if (count($errors) > 0){
			foreach ($errors as $error){
			echo '<p style="color: red">'.$error.'</p';
			}
		}
	}

?>
	<form action="manager_register.php" method="post">
		<center><h1>Registration Form</h1></center>
		<p>First name <br /><input type="text" name="fname" size="30" placeholder="First name" /></p>
		<p>Last name <br /><input type="text" name="lname" size="30" placeholder="Last name" /></p>
		<p>Email <br /><input type="email" name="email" size="60" placeholder="Email" /></p>
		<p>Phone number <br /><input type="text" name="phone" size="30" placeholder="Phone number" /></p>
		<p>Password <br /><input type="password" name="password" size="50" placeholder="Password" /></p>
		<p>Confirm Password <br /><input type="password" name="confirmpassword" size="50" placeholder="Confirm Password" /></p>
		<p><input type="submit" name="register" value="Sign Up" /></p>
	</form>
	<p><a href="manager_login.php">Already have account? </a></p>

<?php
	mysqli_close($dbc);
	include '../include/footer.html';
?>