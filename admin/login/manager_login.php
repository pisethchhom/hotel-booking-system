
<?php
	
	define('TITLE', 'Manager Login');	
	session_start();
	
	include '../include/manager_header.html';
	include '../include/mysqli_connect.php';

	if (isset($_SESSION['admin_name'])) { header('Location: ../reservation/reservation_manage.php'); }

	if (isset($_POST['login'])) {
		$email = mysqli_real_escape_string($dbc, $_POST['email']);
		$password = mysqli_real_escape_string($dbc, $_POST['password']);

		$errors = array();
		if (empty($email)) {array_push($errors, 'Email is required!');}
		if (empty($password)) {array_push($errors, 'Password is required!');}

		if (count($errors) == 0) {
			$pw = md5($password);
			$query = "SELECT * FROM managers WHERE (email = '$email' AND pw = '$pw' AND active = 1)";
			$r = mysqli_query($dbc, $query);
			$user = mysqli_fetch_array($r);

			if ($user) {

				$_SESSION['admin_name'] = $user['id'];

				if (!empty($_POST['rememberme'])) {
					setcookie('loginremember', $username, time() + 3600);
					setcookie('passwordremember', $password, time() + 3600);

				} else {
					if (isset($_COOKIE['loginremember'])) {
						setcookie('loginremember', '');
					}
					if (isset($_COOKIE['passwordremember'])) {
						setcookie('passwordremember', '');
					}
				}
				header('Location: ../reservation/reservation_manage.php');	
			}
			else {
				array_push($errors, 'Wrong Username/Password!');
			}
		}
		if (count($errors) > 0){
			foreach ($errors as $error){
			echo '<p style="color: red">'.$error.'</p';
			}
		}
	}
?>

	<form method="post" action="manager_login.php">
		<center><h1>Login Form</h1></center>
		<p>Email <br /><input type="text" name="email" size="45" placeholder="Email" value="<?php if (isset($_COOKIE["loginremember"])) { echo $_COOKIE["loginremember"]; } ?>" /></p>
		<p>Password <br /><input type="password" name="password" size="45" placeholder="Password" value="<?php if (isset($_COOKIE["passwordremember"])) { echo $_COOKIE["passwordremember"]; } ?>" /></p>
		<p><input type="checkbox" name="rememberme" <?php if(isset($_COOKIE["loginremember"])) { ?> checked <?php } ?> /> Remember Me
		<p><a href="manager_register.php">Not yet have account?</a></p>
		<p><input type="submit" name="login" value="Log In" /></p>
		
<?php
	mysqli_close($dbc);
	include '../include/footer.html';
?>