<?php 
define('TITLE', 'Guest Manage');
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';

?>
</br>
<h1>Guests</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
print '<div style="margin-top: 10px;">';
print '<a href="add_guest.php">Add Guest</a>
<div style="margin-top: 10px;">
<table width="800px;">
		<tr>
			<th>Number</th>
			<th>Guest Name</th>
			<th>Email</th>
			<th>Phone Number</th>
			<th>Passport Number</th>
			<th>Nationality</th>
			<th>State</th>
		</tr>
		';

$query = 'SELECT * FROM guest WHERE active = 1 ORDER BY id';
if ($r = mysqli_query($dbc, $query)) { 

	$count = 0;
	while ($row = mysqli_fetch_array($r)) {
		$count++;
		echo "<tr>
				<td>$count</td>
				<td><a href=\"check_guest.php?id={$row['id']}\">{$row['title']}"." {$row['fname']}"." {$row['lname']}</a></td>
				<td>{$row['email']}</td>
				<td>{$row['phone_no']}</td>
				<td>{$row['passport_no']}</td>
				<td>{$row['nationality']}</td>";

		if ($row['stay'] == 0){
			echo "<td>Waiting</td>";

		} else {
			echo "<td>Staying</td>";
		}
		echo "<td><a href=\"delete_guest.php?id={$row['id']}\">Delete</a></td>
		</tr>";
	}	
	print '</table>'; 

	if ($count == 0){
			echo '<p style="color: red;">There is no data right now!</p>';
	}

	print '<div style="margin-top: 100px;">';	

} else { 
	print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
	<p>The query being run was: ' . $query . '</p>';
} 

mysqli_close($dbc); 
include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>