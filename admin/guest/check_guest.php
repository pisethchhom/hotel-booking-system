<?php 

define('TITLE', 'Guest Information');
session_start();
include ('../include/manager_header.html');
require '../include/mysqli_connect.php';
?>
</br>
<h1>Guest Information</h1>
<?php 
if (isset($_SESSION['admin_name'])) {
	if (isset($_GET['id']) && is_numeric($_GET['id'])) { // Display the entry in a form:
		$check_in = false;
		
		/////////////// print guest information //////////////////
		$query = "SELECT * from guest WHERE id={$_GET['id']}";
		print '<div style="margin-top: 10px;">
		<table width="800px;">
				<tr>
					<th>Guest Name</th>
					<th>Email</th>
					<th>Phone Number</th>
					<th>Passport Number</th>
					<th>Nationality</th>
					<th>State</th>
				</tr>
				';
		if ($r = mysqli_query($dbc, $query)){
			$row_g = mysqli_fetch_array($r);

			print "<tr>
							<td>{$row_g['title']} {$row_g['fname']} {$row_g['lname']}</td>
							<td>{$row_g['email']}</td>
							<td>{$row_g['phone_no']}</td>
							<td>{$row_g['passport_no']}</td>
							<td>{$row_g['nationality']}</td>";
			if ($row_g['stay'] == 1){
				$check_in = true;
				print "<td>Staying</td>";

			} else {
				print "<td>Waiting</td>";
			}
			print "</tr></table>";
		} else { // Couldn't get the information.
			print '<p style="color: red;">Could not retrieve because:<br>' 
			. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
		}
		////////////////////////////////////////////////////////////////

		/////////////////// print reservation information //////////////
		$query = "SELECT * from reservation WHERE guest_id ={$row_g['id']}";
			if ($r = mysqli_query($dbc, $query)){
				$row = mysqli_fetch_array($r);				
				
				print '<div style="margin-top: 10px;">
				<h1 style="color : brown";>Reservation</h1>
				<div style="margin-top: 10px;">';

				if (count((array)$row) > 0){
				print "<table width=\"800px;\">
								<tr>
									<th>Add date</th>
									<th>Add by</th>
									<th>Room Type 1</th>
									<th>Room Type 2</th>
									<th>Room Type 3</th>
									<th>Room Type 4</th>
									<th>In date</th>
									<th>Out date</th>
									<th>Special Offer</th>
								</tr>
								<tr>
								<td>{$row['add_date']}</td>
								<td>{$row['add_by']}</td>
								<td>{$row['rt1']}</td>
								<td>{$row['rt2']}</td>
								<td>{$row['rt3']}</td>
								<td>{$row['rt4']}</td>
								<td>{$row['in_date']}</td>
								<td>{$row['out_date']}</td>
								<td>{$row['special_offer']}</td>
								</tr>
								</table>";
				} else {
					echo "<p>This guest doesn't have any reservation</p>";
				}
			}
		////////////////////////////////////////////////////////////////

		///////////////////// print if guest already checked in ////////
		print '<div style="margin-top: 10px;">
			<h1 style="color : green";>Check in</h1>
			<div style="margin-top: 10px;">';
		if ($check_in) {
			print '<table width="800px;">
			<tr>
				<th>Add date</th>
				<th>Check in</th>
				<th>Check out</th>
			</tr>
			';
			$query = "SELECT * from check_in WHERE guest_id ={$row_g['id']}";
			if ($r = mysqli_query($dbc, $query)){
				$row = mysqli_fetch_array($r);

				print "<tr>
								<td>{$row['insert_date']}</td>
								<td>{$row['check_in']}</td>
								<td>{$row['check_out']}</td>
							</tr></table>";
			} else { // Couldn't get the information.
				print '<p style="color: red;">Could not retrieve because:<br>' 
				. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
			}

			//////////////////// print payment ///////////////////////////////
			print '<div style="margin-top: 10px;">
			<h1 style="color : gray";>Payment</h1>
			<div style="margin-top: 10px;">
			<table width="800px;">
			<tr>
				<th>Add date</th>
				<th>Discount</th>
				<th>Total</th>
				<th>Cash in</th>
				<th>Cash out</th>
			</tr>
			';
			$query = "SELECT * from payment WHERE guest_id ={$row_g['id']}";
			if ($r = mysqli_query($dbc, $query)){
				$row = mysqli_fetch_array($r);
				$symbol = "SELECT symbol from currency_type WHERE id = {$row['currency_type_id']}";
				$r_sym = mysqli_query($dbc, $symbol);
				$row_sym = mysqli_fetch_array($r_sym);

				print "<tr>
								<td>{$row['insert_date']}</td>
								<td>{$row['discount_amount']}%</td>
								<td>{$row['total']}{$row_sym['symbol']}</td>
								<td>{$row['cash_in']}{$row_sym['symbol']}</td>
								<td>{$row['cash_out']}{$row_sym['symbol']}</td>
							</tr></table>";
			} else { // Couldn't get the information.
				print '<p style="color: red;">Could not retrieve because:<br>' 
				. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
			}
			////////////////////////////////////////////////////////////////////

		} else {
			print "<p>This guest haven't check in yet!</p>";
		}
		////////////////////////////////////////////////////////////////

	} // end of check if get id
	else { // No ID received.
		print '<p style="color: red;">This page has been accessed in error.</p>';
	} // End of main IF.

	print '<div style="margin-top: 100px;">';
	mysqli_close($dbc); // Close the connection.
	include ('../include/footer.html');
} else {
	header('Location: ../login/manager_login.php');
}
?>