<?php
	
	define('TITLE', 'Choose Date');
	session_start();
	include 'include/header.html';
	include 'include/mysqli_connect.php';
	
	if (isset($_SESSION['date'])){	unset($_SESSION['date']);	}
	
	if (isset($_SESSION['room'])){	unset($_SESSION['limit_room']);	}
	
	if (isset($_SESSION['guest'])){	unset($_SESSION['guest']);	}

	if (isset($_POST['Submit'])) {

		$start_date = mysqli_real_escape_string($dbc, $_POST['start_date']);
		$end_date = mysqli_real_escape_string($dbc, $_POST['end_date']);

		// saperate start date and end date to arrays 
		$s_date = explode('-', $start_date); // 0=>year 1=>month 2=>day
		$e_date = explode('-', $end_date);

		$errors = array();

		// start date
		if (empty($start_date)) {
			array_push($errors, 'Start date is required!');

		}	elseif((strtotime($start_date) < strtotime('today')) || (strtotime($start_date) === strtotime('today'))){
			array_push($errors, 'Start date is not valid!');
		}

		// if book 2 months before this month 
		if (((int)$s_date[1] > ((int)date("m")+1)) || ((int)$e_date[1] > ((int)date("m")+1))){
			array_push($errors, 'You can only book the room 1 month before this month!');
		}

		// end date
		if (empty($end_date)) {
			array_push($errors, 'End date is required!');

		} else if((strtotime($end_date) < strtotime($start_date)) || (strtotime($end_date) < strtotime('today')) || (strtotime($end_date) === strtotime('today'))){
			array_push($errors, 'End date is not valid!');
		}

		// count error
		if (count($errors) == 0) {

/**
 * This is for checking room available for the period of date
 * This take database of timetable compare each type of room available
 * in the period of data selected by user
 */
		// get the amount of limit room
		$available_room_query = 'SELECT amount_room FROM room_type WHERE active = 1 ORDER BY id';
		if ($r = mysqli_query($dbc, $available_room_query)) { 
			$available_room = array();
			while ($row = mysqli_fetch_array($r)) {
				array_push($available_room, $row[0]);
			}
		} else { 
			print '<p style="color: red;">Could not retrieve the data because:<br>' . mysqli_error($dbc) . '.</p>
			<p>The query being run was: ' . $available_room . '</p>';
		} // end of checking limit room
		
		// check if end date is in another month but the day is less than start date
		if ((int)$s_date[1] < (int)$e_date[1]){ // check if start and end date are in different month 
			$date_limit_query = 'SELECT date_limit FROM month WHERE id = '.(int)$s_date[1].'';
			$r = mysqli_query($dbc, $date_limit_query);
			$limit_date = mysqli_fetch_array($r);
			// set value to end day
			$end_day = (int)$e_date[2] + $limit_date[0];

		} else {
			$end_day = (int)$e_date[2]; // if start date and end date are in the same month 
		}
		$start_day = (int)$s_date[2]; // just set value to start month 
		$day_stay = $end_day - $start_day; // the different between start and end date 
		
		/**
		 * The basic here is to get id of the start date from timetable 
		 * We check from the current id (start date) to the amount of stay
		 * We check for the type available 
		 */

		// get the id of start
		$start_query = 'SELECT id FROM timetable WHERE ((month_id = '.(int)$s_date[1].') AND (date_num = '.$start_day.') AND (active = 1))';
		$r_start = mysqli_query($dbc, $start_query);
		$start = mysqli_fetch_array($r_start); // start from id of start date
		$id = $start[0];

		// set variables
		$limit_rt1 = $available_room[0];
		$limit_rt2 = $available_room[1];
		$limit_rt3 = $available_room[2];
		$limit_rt4 = $available_room[3];
		$rt1 = 0;
		$rt2 = 0;
		$rt3 = 0;
		$rt4 = 0;

		// check each data for available type 
		for ($i=0; $i<$day_stay; $i++){
			$query = 'SELECT month_id, date_num, rt1, rt2, rt3, rt4 FROM timetable WHERE ((id = '.$id.') AND (active = 1))';
			$r = mysqli_query($dbc, $query);
			$row = mysqli_fetch_array($r);

			if($row['rt1'] > $rt1){	$rt1 = $row['rt1'];}

			if($row['rt2'] > $rt2){	$rt2 = $row['rt2'];}

			if($row['rt3'] > $rt3){	$rt3 = $row['rt3'];}

			if($row['rt4'] > $rt4){ $rt4 = $row['rt4'];}
			// if this day all rooms are full, store this busy day
			if (($row['rt1'] >= $limit_rt1) && ($row['rt2'] >= $limit_rt2) && ($row['rt3'] >= $limit_rt3) && ($row['rt4'] >= $limit_rt4)){
				echo '<p style="color: red">(Date: '.$row['date_num'].', Month: '.$row['month_id'].') No room available on that day!</p>';
			}
			++$id;
		}
		$available_room[0] -= $rt1;
		$available_room[1] -= $rt2;
		$available_room[2] -= $rt3;
		$available_room[3] -= $rt4;
		
/////////////////////////// end of checking and set for available room

			if (($available_room[0] != 0) || ($available_room[1] != 0) || ($available_room[2] != 0) || ($available_room[3] != 0)){
				$date_array = array($start_date, $end_date,$day_stay);
				$_SESSION['room'] = $available_room;
				$_SESSION['date'] = $date_array;
				header('Location: choose_roomType.php');
			} else {
				echo '<p style="color: red">There is no available room suitable for that period!</p>';
			}
		} else if (count($errors) > 0){
			foreach ($errors as $error){
				echo '<p style="color: red">'.$error.'</p>';
				}
			}
}
?>
	<form action="choose_date.php" method="post">
		<center><h1>Choose date</h1></center>
		<p style="color: blue">Note: you can only book the room 1 month before this month!</p>
		<p>Start Date<br /><input type="date" min="<?php 

		$date = new DateTime(date('Y-m-d'));
		$date->modify('+1 day');
		echo $date->format('Y-m-d');

		?>" max="<?php

		$date = new DateTime(date('Y-m-d'));
		$date->modify('+1 month');
		$date->modify('last day of this month');
		echo $date->format('Y-m-d');

		?>" name="start_date"/></p>
		<p>End Date<br /><input type="date" min="<?php 

		$date = new DateTime(date('Y-m-d'));
		$date->modify('+1 day');
		echo $date->format('Y-m-d');

		?>" max="<?php

		$date = new DateTime(date('Y-m-d'));
		$date->modify('+1 month');
		$date->modify('last day of this month');
		echo $date->format('Y-m-d');

		?>" name="end_date"/></p>
		<p><input type="submit" name="Submit" value="Check" /></p>
	</form>

<?php
	mysqli_close($dbc);
	include 'include/footer.html';
?>