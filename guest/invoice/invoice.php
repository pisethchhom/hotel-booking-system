<?php
require('fpdf17/fpdf.php');
session_start();
require '../include/mysqli_connect.php';

$room_type = 'SELECT price from room_type order by id';
$r = mysqli_query($dbc,$room_type);
//A4 width : 219mm
//default margin : 10mm each side
//writable horizontal : 219-(10*2)=189mm

$pdf = new FPDF('P','mm','A4');

$pdf->AddPage();

//set font to arial, bold, 14pt
$pdf->SetFont('Arial','B',14);

//Cell(width , height , text , border , end line , [align] )

$pdf->Cell(130	,5,'iHotel',0,0);

$pdf->SetFont('Arial','B',20);
$pdf->Cell(59	,5,'Reservation Info',0,1);//end of line

//set font to arial, regular, 12pt
$pdf->SetFont('Arial','',10);

$pdf->Cell(130	,5,'Street: st.999',0,0);
$pdf->Cell(59	,5,'',0,1);//end of line

$pdf->Cell(130	,5,'Phnom Penh, Cambodia',0,0);
$pdf->Cell(25	,5,'Date',0,0);
$pdf->Cell(34	,5,date('D-d/M/Y'),0,1);//end of line

$pdf->Cell(130	,5,'Phone [+85512345678]',0,0);

$pdf->Cell(25	,5,'Invoice #',0,0);
$pdf->Cell(34	,5,$_SESSION['reservation'][0],0,1);//end of line


$pdf->Cell(25	,5,'Customer ID #',0,0);
$pdf->Cell(34	,5,$_SESSION['reservation'][2],0,1);//end of line

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

$pdf->SetFont('Arial','B',14);

//billing address
$pdf->Cell(100	,5,'Guest',0,1);//end of line

$pdf->SetFont('Arial','',10);

//add dummy cell at beginning of each line for indentation
$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,"Name: {$_SESSION['guest'][0]} {$_SESSION['guest'][1]} {$_SESSION['guest'][2]}",0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,'Nationality: '.$_SESSION['guest'][6],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,'Email: '.$_SESSION['guest'][3],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,'Phone: '.$_SESSION['guest'][4],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,'Passport Number: '.$_SESSION['guest'][5],0,1);

$pdf->Cell(10	,5,'',0,0);
$pdf->Cell(90	,5,'**Please save this pdf file to confirm at the hotel**',0,1);

//make a dummy empty cell as a vertical spacer
$pdf->Cell(189	,10,'',0,1);//end of line

//invoice contents
$pdf->SetFont('Arial','B',12);

$pdf->Cell(130	,6,'Description',1,0,'C');
// $pdf->Cell(25	,5,'Date',1,0);
$pdf->Cell(59	,6,'Date',1,1,'C');//end of line

$pdf->SetFont('Arial','',9);

//Numbers are right-aligned so we give 'R' after new line parameter

$pdf->Cell(130	,6,'In date',1,0);
$date = new DateTime($_SESSION['date'][0]);
$pdf->Cell(59	,6,$date->format('D-d/M/Y'),1,1,'R');//end of line

$pdf->Cell(130	,6,'Out date',1,0);
$date = new DateTime($_SESSION['date'][1]);
$pdf->Cell(59	,6,$date->format('D-d/M/Y'),1,1,'R');//end of line

$pdf->Cell(130	,6,'',0,0);
$pdf->Cell(59	,6,'',0,1);

$pdf->SetFont('Arial','B',12);

$pdf->Cell(100	,6,'Room Type',1,0,'C');
$pdf->Cell(20	,6,'Amount',1,0,'C');
$pdf->Cell(40	,6,'Price per Room',1,0,'C');
$pdf->Cell(29	,6,'Price',1,1,'C');//end of line

$pdf->SetFont('Arial','',9);

$pdf->Cell(100	,6,'STANDARD SINGLE',1,0);
if (isset($_SESSION['room'][0])){
  $amount = $_SESSION['room'][0];
} else {
  $amount = 0;
}
$pdf->Cell(20	,6,$amount,1,0);
$row = mysqli_fetch_array($r);
$price_ty1 = $row[0];
$pdf->Cell(40	,6,$price_ty1.'$',1,0,'R');
$pdf->Cell(29	,6,(string)($_SESSION['room'][0]*$row[0]).'$',1,1,'R');//end of line

$pdf->Cell(100	,6,'DELUXE DOUBLE ROOM',1,0);
if (isset($_SESSION['room'][1])){
  $amount = $_SESSION['room'][1];
} else {
  $amount = 0;
}
$pdf->Cell(20	,6,$amount,1,0);
$row = mysqli_fetch_array($r);
$price_ty2 = $row[0];
$pdf->Cell(40	,6,$price_ty2.'$',1,0,'R');
$pdf->Cell(29	,6,(string)($_SESSION['room'][1]*$row[0]).'$',1,1,'R');//end of line

$pdf->Cell(100	,6,'SUPERIOR TWIN',1,0);
if (isset($_SESSION['room'][2])){
  $amount = $_SESSION['room'][2];
} else {
  $amount = 0;
}
$pdf->Cell(20	,6,$amount,1,0);
$row = mysqli_fetch_array($r);
$price_ty3 = $row[0];
$pdf->Cell(40	,6,$price_ty3.'$',1,0,'R');
$pdf->Cell(29	,6,(string)($_SESSION['room'][2]*$row[0]).'$',1,1,'R');//end of line

$pdf->Cell(100	,6,'FAMILY SUITE',1,0);
if (isset($_SESSION['room'][3])){
  $amount = $_SESSION['room'][3];
} else {
  $amount = 0;
}
$pdf->Cell(20	,6,$amount,1,0);
$row = mysqli_fetch_array($r);
$price_ty4 = $row[0];
$pdf->Cell(40	,6,$price_ty4.'$',1,0,'R');
$pdf->Cell(29	,6,(string)($_SESSION['room'][3]*$row[0]).'$',1,1,'R');//end of line

//summary
$pdf->Cell(131	,6,'',0,0);
$pdf->Cell(25	,6,'Subtotal',0,0,'C');
$pdf->Cell(4	,6,'',0,0);
$price = 0;
$i = 0;
while($i < 4){
  switch($i){
    case 0: $price += ($_SESSION['room'][$i] * $price_ty1); break;
    case 1: $price += ($_SESSION['room'][$i] * $price_ty2); break;
    case 2: $price += ($_SESSION['room'][$i] * $price_ty3); break;
    case 3: $price += ($_SESSION['room'][$i] * $price_ty4); break;
  }
  ++$i;
}
$pdf->Cell(29	,6,$price.'$',1,1,'R');//end of line

$pdf->Cell(131	,6,'',0,0);
$pdf->Cell(25	,6,'Stay',0,0,'C');
$pdf->Cell(4	,6,'x',1,0);
if ($_SESSION['date'][2] > 0){
  $stay = ' days';
} else {
  $stay = ' day';
}
$pdf->Cell(29	,6,$_SESSION['date'][2].$stay,1,1,'R');//end of line

$pdf->Cell(131	,6,'',0,0);
$pdf->Cell(25	,6,'Total',0,0,'C');
$pdf->Cell(4	,6,'',0,0);
$pdf->Cell(29	,6,$_SESSION['reservation'][1].'$',1,1,'R');//end of line

$filename="../../admin/pdf/reservation_info/reservation_id{$_SESSION['reservation'][0]}.pdf";
$pdf->Output($filename,'F');

$pdf->Output();
mysqli_close($dbc);
?>
