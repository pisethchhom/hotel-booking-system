<?php 

$page_title = 'Choose Room Type';
session_start();
include ('include/header.html');
require 'include/mysqli_connect.php';
?>
</br>
<center>
<h1>Choose your room type</h1>
</center>
<?php
if (isset($_POST['Submit'])) {
	$booked_room = array($_POST['book_type1'],$_POST['book_type2'],$_POST['book_type3'],$_POST['book_type4']);
	$_SESSION['room'] = $booked_room;
	header('Location: add_info.php');
}
print '<div style="margin-top: 20px;">';
print '<form action=\'choose_roomType.php\' method=\'post\'>
		<table width="800px;">
		<tr>
			<th>Type</th>
			<th>Average Capacity</th>
			<th>Description</th>
			<th>Price</th>
			<th>Available</th>
			<th>Amount Book</th>
		</tr>
		';

if ($_SESSION['room'][0] > 0){
	$room_type1 = "SELECT rt.name, rt.capacity, rt.info, rt.price, ct.symbol FROM room_type rt 
	JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = 1 AND rt.active = 1)";
	$r1 = mysqli_query($dbc, $room_type1);
	$row_type1 = mysqli_fetch_array($r1);
	print "
	<tr>
		<td>{$row_type1['name']}</td>
		<td>{$row_type1['capacity']}</td>
		<td>{$row_type1['info']}</td>
		<td>".$row_type1['price']." ".$row_type1['symbol']."</td>
		<td>{$_SESSION['room'][0]}</td>
		<td><select name='book_type1'>";

		for ($i=0; $i<=$_SESSION['room'][0]; $i++){
			print "<option value=$i>$i</option>";
		}
		print "</select></td>
	 </tr>";
}
if ($_SESSION['room'][1] > 0){
	$room_type2 = 'SELECT * FROM room_type rt JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = 2 AND rt.active = 1)';
	$r2 = mysqli_query($dbc, $room_type2);
	$row_type2 = mysqli_fetch_array($r2);
	print "
	<tr>
		<td>{$row_type2['name']}</td>
		<td>{$row_type2['capacity']}</td>
		<td>{$row_type2['info']}</td>
		<td>".$row_type2['price']." ".$row_type2['symbol']."</td>
		<td>{$_SESSION['room'][1]}</td>
		<td><select name='book_type2'>";

		for ($i=0; $i<=$_SESSION['room'][1]; $i++){
			print "<option value=$i>$i</option>";
		}
		print "</select></td>
	 </tr>";
}
if ($_SESSION['room'][2] > 0){
	$room_type3 = 'SELECT * FROM room_type rt JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = 3 AND rt.active = 1)';
	$r3 = mysqli_query($dbc, $room_type3);
	$row_type3 = mysqli_fetch_array($r3);
	print "
	<tr>
		<td>{$row_type3['name']}</td>
		<td>{$row_type3['capacity']}</td>
		<td>{$row_type3['info']}</td>
		<td>".$row_type3['price']." ".$row_type3['symbol']."</td>
		<td>{$_SESSION['room'][2]}</td>
		<td><select name='book_type3'>";

		for ($i=0; $i<=$_SESSION['room'][2]; $i++){
			print "<option value=$i>$i</option>";
		}
		print "</select></td>
	 </tr>";
}
if ($_SESSION['room'][3] > 0){
	$room_type4 = 'SELECT * FROM room_type rt JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = 4 AND rt.active = 1)';
	$r4 = mysqli_query($dbc, $room_type4);
	$row_type4 = mysqli_fetch_array($r4);
	print "
	<tr>
		<td>{$row_type4['name']}</td>
		<td>{$row_type4['capacity']}</td>
		<td>{$row_type4['info']}</td>
		<td>".$row_type4['price']." ".$row_type4['symbol']."</td>
		<td>{$_SESSION['room'][3]}</td>
		<td><select name='book_type4'>";

		for ($i=0; $i<=$_SESSION['room'][3]; $i++){
			print "<option value=$i>$i</option>";
		}
		print "</select></td>
	 </tr>";
}
print '</table>
<input type=\'submit\' name=\'Submit\' value=\'next\' />
</form>'; 

mysqli_close($dbc); 
include ('include/footer.html');
?>