<?php 

$page_title = 'Reservation Summery';
session_start();
include ('include/header.html');
require 'include/mysqli_connect.php';

print "<p style=\"color: blue\">Please notice if there is an error!</p>
</br>
<h1>Booking Summery</h1>";

$title = mysqli_real_escape_string($dbc, $_SESSION['guest'][0]);
$fname = mysqli_real_escape_string($dbc, $_SESSION['guest'][1]);
$lname = mysqli_real_escape_string($dbc, $_SESSION['guest'][2]);
$email = mysqli_real_escape_string($dbc, $_SESSION['guest'][3]);
$phone_no = mysqli_real_escape_string($dbc, $_SESSION['guest'][4]);
$pass_no = mysqli_real_escape_string($dbc, $_SESSION['guest'][5]);
$nationality = mysqli_real_escape_string($dbc, $_SESSION['guest'][6]);
$comment = mysqli_real_escape_string($dbc, $_SESSION['guest'][7]);

if (isset($_POST['Submit'])) {

  // add guest
	$guest = "INSERT INTO guest VALUES (NULL,NULL,'$title','$fname','$lname','$email','$phone_no','$pass_no','$nationality',0,1)";

	if (mysqli_query($dbc, $guest)) {
		$guest_select = "SELECT id from guest WHERE email = '$email'";

			if ($r = mysqli_query($dbc, $guest_select)) {
				$guest_row = mysqli_fetch_assoc($r);

				// check if guest chose 0 room of roomtype, the set variable to 0
				// room type 1 
				if ($_SESSION['room'][0] == 0){
					$rt1 = 0;
				} else {
					$rt1 = $_SESSION['room'][0];
				}

				// room type 2
				if ($_SESSION['room'][1] == 0){
					$rt2 = 0;
				} else {
					$rt2 = $_SESSION['room'][1];
				}

				// room type 3
				if ($_SESSION['room'][2] == 0){
					$rt3 = 0;
				} else {
					$rt3 = $_SESSION['room'][2];
				}

				// room type 4
				if ($_SESSION['room'][3] == 0){
					$rt4 = 0;
				} else {
					$rt4 = $_SESSION['room'][3];
				}

				$query = "INSERT INTO reservation VALUES(NULL,NULL,'webpage',{$guest_row['id']}, $rt1, $rt2, $rt3, $rt4,
				'{$_SESSION['date'][0]}','{$_SESSION['date'][1]}',{$_POST['total']},{$_POST['symbol_id']},'$comment', 1)";
				
				if (mysqli_query($dbc, $query)){
					$s_date = explode('-', $_SESSION['date'][0]);
					$start_query = 'SELECT id FROM timetable WHERE ((month_id = '.(int)$s_date[1].') AND (date_num = '.$s_date[2].') AND (active = 1))';
					$r_start = mysqli_query($dbc, $start_query);
					$start = mysqli_fetch_array($r_start); // start from id of start date
					$id = $start[0];

					for ($i=0; $i<$_SESSION['date'][2]; $i++){
						$update_timetable = "UPDATE timetable set rt1 = rt1 + ".(int)$_SESSION['room'][0].", rt2 = rt2 + ".(int)$_SESSION['room'][1].", 
						rt3 = rt3 + ".(int)$_SESSION['room'][2].", rt4 = rt4 + ".(int)$_SESSION['room'][3]." WHERE id =".($id + $i)."";

						if (mysqli_query($dbc, $update_timetable)){
							$reservation = "SELECT id from reservation WHERE guest_id = {$guest_row['id']}";
							$r = mysqli_query($dbc, $reservation);
							$re_row = mysqli_fetch_assoc($r);
							$_SESSION['reservation'] = array($re_row['id'], $_POST['total'], $guest_row['id']);
							print '<p>Your booking is done!</p>';
							header('Location: invoice/invoice.php');

						}else {
						print '<p style="color: red;">Could not add the entry because:<br>'
						. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $update_timetable . '</p>';
						}
					}
				} else {
					print '<p style="color: red;">Could not add the entry because:<br>' 
					. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $query . '</p>';
				}
		} else {
			print '<p style="color: red;">Could not add the entry because:<br>' 
			. mysqli_error($dbc) . '.</p><p>The query being run was: ' . $guest_select . '</p>';
		}
	}
}
print '<div style="margin-top: 20px;">';

print '
<table width="800px;">
		<tr>
			<th>Guest Name</th>
			<th>Email</th>
			<th>Start Date</th>
			<th>End Date</th>
		</tr>
		';

print "
		<tr>
			<td>".$_SESSION['guest'][0].$_SESSION['guest'][1]." ".$_SESSION['guest'][2]."</td>
			<td>".$_SESSION['guest'][3]."</td>
			<td>".$_SESSION['date'][0]."</td>
			<td>".$_SESSION['date'][1]."</td>
		</tr>
		";

print '
		<tr>
			<th>Room</th>
			<th>Amount</th>
			<th>Price per room</th>
			<th>Day Stay</th>
			<th>Total</th>
		</tr>';
$i = 1;
$total = 0;
foreach ($_SESSION['room'] as $room){
	if (($room > 0) && ($i < 5)){
		$room_query = "SELECT rt.name, rt.price, ct.symbol, ct.id as symbol_id FROM room_type rt 
		JOIN currency_type ct ON rt.currency_type_id = ct.id WHERE (rt.id = $i AND rt.active = 1)";
		$r = mysqli_query($dbc, $room_query);
		$row = mysqli_fetch_array($r);
		$price = ($row['price'] * $room) * $_SESSION['date'][2];
		print "
		<tr>
			<td>{$row['name']}</td>
			<td>$room</td>
			<td>".$row['price']." ".$row['symbol']."</td>
			<td>".$_SESSION['date'][2]."</td>
			<td>".$price." ".$row['symbol']."</td>
		</tr>";
		$total += $price;
	}
	++$i;
}

print "<tr>
		<th>Total: </th>
		<th></th>
		<th></th>
		<th>$total {$row['symbol']}</th>
		</tr>
		</table>
		<form action=\"booking_summery.php\" method=\"post\">
		<input type=\"hidden\" name=\"total\" value=". $total . ">
		<input type=\"hidden\" name=\"symbol_id\" value=". $row['symbol_id'] . ">
		<input type=\"submit\" name=\"Submit\" value=\"Done\" /></p>
		</form>
		";
mysqli_close($dbc); 
include ('include/footer.html');
?>