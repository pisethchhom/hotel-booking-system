-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2018 at 06:47 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `booking_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `check_in`
--

CREATE TABLE `check_in` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guest_id` int(10) UNSIGNED NOT NULL,
  `check_in` date NOT NULL,
  `check_out` date NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `check_in`
--

INSERT INTO `check_in` (`id`, `insert_date`, `guest_id`, `check_in`, `check_out`, `payment_id`, `active`) VALUES
(10, '2018-06-03 15:13:04', 9, '2018-06-03', '2018-06-05', 10, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `currency_type`
--

CREATE TABLE `currency_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(20) NOT NULL,
  `symbol` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency_type`
--

INSERT INTO `currency_type` (`id`, `insert_date`, `type`, `symbol`, `active`) VALUES
(1, '2018-04-27 07:09:52', 'Riel', '៛', b'1'),
(2, '2018-04-27 07:09:52', 'Dollar', '$', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` enum('Mr.','Miss.','Mrs.') NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `passport_no` varchar(30) NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `stay` bit(1) DEFAULT b'0',
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`id`, `insert_date`, `title`, `fname`, `lname`, `email`, `phone_no`, `passport_no`, `nationality`, `stay`, `active`) VALUES
(8, '2018-06-04 06:56:49', 'Mr.', 'A', 'B', 'ab@gmail.com', '012999999', '9999', 'american', b'0', b'0'),
(9, '2018-06-03 13:54:02', 'Mr.', 'jammy', 'john', 'jam@gmail.com', '0909090909', '0909009', 'bahraini', b'1', b'1'),
(11, '2018-06-03 12:42:00', 'Mr.', 'first', 'second', 'firstandsecond@gmail.com', '099288348', '820383', 'austrian', b'0', b'1'),
(12, '2018-06-04 06:56:28', 'Mr.', 'john', 'john2', 'john@gmail.com', '91829', '919191', 'bahamian', b'0', b'0'),
(13, '2018-06-04 06:56:20', 'Mr.', 'aaaaa', 'aaa', 'aaaa@gmail.com', '121', '11', 'armenian', b'0', b'0'),
(14, '2018-06-04 06:56:16', 'Mr.', 'aaaaa', 'aaa', 'aaaaaa@gmail.com', '1111', '11', 'azerbaijani', b'0', b'0'),
(15, '2018-06-04 06:56:11', 'Mr.', 'h', 'h', 'h@gmail.com', '99', '99', 'batswana', b'0', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

CREATE TABLE `managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `pw` varchar(50) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `managers`
--

INSERT INTO `managers` (`id`, `insert_date`, `fname`, `lname`, `email`, `phone_no`, `pw`, `register_date`, `active`) VALUES
(1, '2018-04-27 07:09:53', 'a', 'a', 'a@a.com', '1', '0cc175b9c0f1b6a831c399e269772661', '2018-04-27 07:01:40', b'1'),
(2, '2018-04-27 07:09:53', 'admin', '2', 'admin@gmail.com', '123', '202cb962ac59075b964b07152d234b70', '2018-04-27 07:01:40', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `month`
--

CREATE TABLE `month` (
  `id` smallint(6) NOT NULL,
  `name` varchar(20) NOT NULL,
  `date_limit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `month`
--

INSERT INTO `month` (`id`, `name`, `date_limit`) VALUES
(1, 'January', 31),
(2, 'February', 29),
(3, 'March', 31),
(4, 'April', 30),
(5, 'May', 31),
(6, 'June', 30),
(7, 'July', 31),
(8, 'August', 31),
(9, 'September', 30),
(10, 'October', 31),
(11, 'November', 30),
(12, 'December', 31);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guest_id` int(10) UNSIGNED NOT NULL,
  `discount_amount` int(10) UNSIGNED NOT NULL,
  `total` int(10) UNSIGNED NOT NULL,
  `cash_in` int(10) UNSIGNED NOT NULL,
  `cash_out` int(10) UNSIGNED NOT NULL,
  `currency_type_id` int(10) UNSIGNED DEFAULT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `insert_date`, `guest_id`, `discount_amount`, `total`, `cash_in`, `cash_out`, `currency_type_id`, `active`) VALUES
(10, '2018-06-03 15:25:26', 9, 0, 840, 840, 0, 2, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(10) UNSIGNED NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `add_by` varchar(50) DEFAULT NULL,
  `guest_id` int(10) UNSIGNED NOT NULL,
  `rt1` int(11) DEFAULT NULL,
  `rt2` int(11) DEFAULT NULL,
  `rt3` int(11) DEFAULT NULL,
  `rt4` int(11) DEFAULT NULL,
  `in_date` date NOT NULL,
  `out_date` date NOT NULL,
  `total` int(11) NOT NULL,
  `currency_type_id` int(10) UNSIGNED NOT NULL,
  `special_offer` varchar(100) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `add_date`, `add_by`, `guest_id`, `rt1`, `rt2`, `rt3`, `rt4`, `in_date`, `out_date`, `total`, `currency_type_id`, `special_offer`, `active`) VALUES
(2, '2018-06-03 15:13:03', 'webpage', 9, 0, 2, 0, 3, '2018-06-04', '2018-06-08', 840, 2, '', b'0'),
(3, '2018-06-03 14:26:29', 'webpage', 11, 2, 0, 0, 1, '2018-06-05', '2018-06-09', 320, 2, '', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rooms_no` varchar(5) NOT NULL,
  `floor_no` varchar(10) NOT NULL,
  `room_type_id` int(10) UNSIGNED NOT NULL,
  `description` text,
  `available` bit(1) NOT NULL,
  `check_in_id` int(10) UNSIGNED DEFAULT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `insert_date`, `rooms_no`, `floor_no`, `room_type_id`, `description`, `available`, `check_in_id`, `active`) VALUES
(1, '2018-06-03 14:25:37', '001', 'ground', 1, '', b'1', NULL, b'1'),
(2, '2018-06-03 14:25:37', '002', 'ground', 1, '', b'1', NULL, b'1'),
(3, '2018-06-03 00:07:24', '003', 'ground', 1, NULL, b'1', NULL, b'1'),
(4, '2018-06-03 15:13:04', '011', 'ground', 2, NULL, b'0', 10, b'1'),
(5, '2018-06-03 14:25:37', '012', 'ground', 2, NULL, b'1', NULL, b'1'),
(6, '2018-06-03 15:13:04', '013', 'ground', 2, NULL, b'0', 10, b'1'),
(7, '2018-06-03 00:07:24', '021', 'ground', 3, NULL, b'1', NULL, b'1'),
(8, '2018-06-03 00:07:24', '022', 'ground', 3, NULL, b'1', NULL, b'1'),
(9, '2018-06-03 00:07:24', '023', 'ground', 3, NULL, b'1', NULL, b'1'),
(10, '2018-06-03 15:13:04', '031', 'ground', 4, NULL, b'0', 10, b'1'),
(11, '2018-06-03 15:13:04', '032', 'ground', 4, NULL, b'0', 10, b'1'),
(12, '2018-04-28 13:13:32', '033', 'ground', 4, NULL, b'1', NULL, b'1'),
(13, '2018-06-03 00:07:24', '101', '1', 1, NULL, b'1', NULL, b'1'),
(14, '2018-06-03 00:07:24', '102', '1', 1, NULL, b'1', NULL, b'1'),
(15, '2018-06-03 00:07:24', '103', '1', 1, NULL, b'1', NULL, b'1'),
(16, '2018-04-28 13:13:32', '111', '1', 2, NULL, b'1', NULL, b'1'),
(17, '2018-04-28 13:13:32', '112', '1', 2, NULL, b'1', NULL, b'1'),
(18, '2018-04-28 13:13:32', '113', '1', 2, NULL, b'1', NULL, b'1'),
(19, '2018-06-03 00:07:24', '121', '1', 3, NULL, b'1', NULL, b'1'),
(20, '2018-06-03 00:07:24', '122', '1', 3, NULL, b'1', NULL, b'1'),
(21, '2018-06-03 00:07:24', '123', '1', 3, NULL, b'1', NULL, b'1'),
(22, '2018-06-03 15:13:04', '131', '1', 4, NULL, b'0', 10, b'1'),
(23, '2018-04-28 13:13:32', '132', '1', 4, NULL, b'1', NULL, b'1'),
(24, '2018-06-03 14:25:37', '133', '1', 4, NULL, b'1', NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `room_type`
--

CREATE TABLE `room_type` (
  `id` int(10) UNSIGNED NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(20) NOT NULL,
  `capacity` int(10) UNSIGNED NOT NULL,
  `info` text,
  `currency_type_id` int(10) UNSIGNED NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `amount_room` int(11) NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `room_type`
--

INSERT INTO `room_type` (`id`, `insert_date`, `name`, `capacity`, `info`, `currency_type_id`, `price`, `amount_room`, `active`) VALUES
(1, '2018-05-27 06:40:37', 'STANDARD SINGLE', 1, '1 simple bed', 2, 15, 6, b'1'),
(2, '2018-05-24 01:44:16', 'DELUXE DOUBLE ROOM', 2, 'couple room', 2, 30, 6, b'1'),
(3, '2018-05-24 01:44:16', 'SUPERIOR TWIN', 2, '2 beds', 2, 30, 6, b'1'),
(4, '2018-05-24 01:44:16', 'FAMILY SUITE', 4, 'family room', 2, 50, 6, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `timetable`
--

CREATE TABLE `timetable` (
  `id` smallint(6) NOT NULL,
  `month_id` smallint(6) NOT NULL,
  `date_num` int(11) NOT NULL,
  `rt1` int(11) NOT NULL,
  `rt2` int(11) NOT NULL,
  `rt3` int(11) NOT NULL,
  `rt4` int(11) NOT NULL,
  `active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timetable`
--

INSERT INTO `timetable` (`id`, `month_id`, `date_num`, `rt1`, `rt2`, `rt3`, `rt4`, `active`) VALUES
(3, 5, 26, 1, 0, 0, 0, b'0'),
(4, 5, 27, 0, 1, 0, 0, b'0'),
(5, 5, 28, 0, 0, 1, 0, b'0'),
(6, 5, 29, 0, 0, 0, 1, b'0'),
(7, 5, 30, 0, 0, 0, 1, b'0'),
(8, 5, 31, 0, 1, 0, 0, b'0'),
(9, 6, 1, 0, 1, 0, 0, b'0'),
(10, 6, 2, 6, 0, 6, 0, b'0'),
(11, 6, 3, 6, 0, 6, 0, b'0'),
(12, 6, 4, 6, 2, 6, 3, b'0'),
(13, 6, 5, 6, 3, 6, 4, b'0'),
(14, 6, 6, 3, 3, 0, 4, b'1'),
(15, 6, 7, 3, 3, 0, 4, b'1'),
(16, 6, 8, 2, 0, 0, 1, b'1'),
(17, 6, 9, 0, 0, 0, 0, b'1'),
(18, 6, 10, 0, 0, 0, 0, b'1'),
(19, 6, 11, 0, 0, 0, 0, b'1'),
(20, 6, 12, 0, 0, 0, 0, b'1'),
(21, 6, 13, 0, 0, 0, 0, b'1'),
(22, 6, 14, 0, 0, 0, 0, b'1'),
(23, 6, 15, 0, 0, 0, 0, b'1'),
(24, 6, 16, 0, 0, 0, 0, b'1'),
(25, 6, 17, 0, 0, 0, 0, b'1'),
(26, 6, 18, 0, 0, 0, 0, b'1'),
(27, 6, 19, 0, 0, 0, 0, b'1'),
(28, 6, 20, 0, 0, 0, 0, b'1'),
(29, 6, 21, 0, 0, 0, 0, b'1'),
(30, 6, 22, 0, 0, 0, 0, b'1'),
(31, 6, 23, 0, 0, 0, 0, b'1'),
(32, 6, 24, 0, 0, 0, 0, b'1'),
(33, 6, 25, 0, 0, 0, 0, b'1'),
(34, 6, 26, 0, 0, 0, 0, b'1'),
(35, 6, 27, 0, 0, 0, 0, b'1'),
(36, 6, 28, 0, 0, 0, 0, b'1'),
(37, 6, 29, 0, 0, 0, 0, b'1'),
(38, 6, 30, 0, 0, 0, 0, b'1'),
(39, 7, 1, 0, 0, 0, 0, b'1'),
(40, 7, 2, 0, 0, 0, 0, b'1'),
(41, 7, 3, 0, 0, 0, 0, b'1'),
(42, 7, 4, 0, 0, 0, 0, b'1'),
(43, 7, 5, 0, 0, 0, 0, b'1'),
(44, 7, 6, 0, 0, 0, 0, b'1'),
(45, 7, 7, 0, 0, 0, 0, b'1'),
(46, 7, 8, 0, 0, 0, 0, b'1'),
(47, 7, 9, 0, 0, 0, 0, b'1'),
(48, 7, 10, 0, 0, 0, 0, b'1'),
(49, 7, 11, 0, 0, 0, 0, b'1'),
(50, 7, 12, 0, 0, 0, 0, b'1'),
(51, 7, 13, 0, 0, 0, 0, b'1'),
(52, 7, 14, 0, 0, 0, 0, b'1'),
(53, 7, 15, 0, 0, 0, 0, b'1'),
(54, 7, 16, 0, 0, 0, 0, b'1'),
(55, 7, 17, 0, 0, 0, 0, b'1'),
(56, 7, 18, 0, 0, 0, 0, b'1'),
(57, 7, 19, 0, 0, 0, 0, b'1'),
(58, 7, 20, 0, 0, 0, 0, b'1'),
(59, 7, 21, 0, 0, 0, 0, b'1'),
(60, 7, 22, 0, 0, 0, 0, b'1'),
(61, 7, 23, 0, 0, 0, 0, b'1'),
(62, 7, 24, 0, 0, 0, 0, b'1'),
(63, 7, 25, 0, 0, 0, 0, b'1'),
(64, 7, 26, 0, 0, 0, 0, b'1'),
(65, 7, 27, 0, 0, 0, 0, b'1'),
(66, 7, 28, 0, 0, 0, 0, b'1'),
(67, 7, 29, 0, 0, 0, 0, b'1'),
(68, 7, 30, 0, 0, 0, 0, b'1'),
(69, 7, 31, 0, 0, 0, 0, b'1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `check_in`
--
ALTER TABLE `check_in`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guest_id` (`guest_id`),
  ADD KEY `payment_id` (`payment_id`);

--
-- Indexes for table `currency_type`
--
ALTER TABLE `currency_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `managers`
--
ALTER TABLE `managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `month`
--
ALTER TABLE `month`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_ibfk_1` (`guest_id`),
  ADD KEY `payment_ibfk_2` (`currency_type_id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guest_id` (`guest_id`),
  ADD KEY `currency_type_id` (`currency_type_id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rooms_ibfk_1` (`room_type_id`),
  ADD KEY `rooms_ibfk_3` (`check_in_id`);

--
-- Indexes for table `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_type_id` (`currency_type_id`);

--
-- Indexes for table `timetable`
--
ALTER TABLE `timetable`
  ADD PRIMARY KEY (`id`),
  ADD KEY `month_id` (`month_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `check_in`
--
ALTER TABLE `check_in`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `currency_type`
--
ALTER TABLE `currency_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `guest`
--
ALTER TABLE `guest`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `managers`
--
ALTER TABLE `managers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `month`
--
ALTER TABLE `month`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `timetable`
--
ALTER TABLE `timetable`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `check_in`
--
ALTER TABLE `check_in`
  ADD CONSTRAINT `check_in_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`),
  ADD CONSTRAINT `check_in_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`),
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`currency_type_id`) REFERENCES `currency_type` (`id`);

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `reservation_ibfk_1` FOREIGN KEY (`guest_id`) REFERENCES `guest` (`id`),
  ADD CONSTRAINT `reservation_ibfk_2` FOREIGN KEY (`currency_type_id`) REFERENCES `currency_type` (`id`);

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`room_type_id`) REFERENCES `room_type` (`id`),
  ADD CONSTRAINT `rooms_ibfk_3` FOREIGN KEY (`check_in_id`) REFERENCES `check_in` (`id`);

--
-- Constraints for table `room_type`
--
ALTER TABLE `room_type`
  ADD CONSTRAINT `room_type_ibfk_1` FOREIGN KEY (`currency_type_id`) REFERENCES `currency_type` (`id`);

--
-- Constraints for table `timetable`
--
ALTER TABLE `timetable`
  ADD CONSTRAINT `timetable_ibfk_1` FOREIGN KEY (`month_id`) REFERENCES `month` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
